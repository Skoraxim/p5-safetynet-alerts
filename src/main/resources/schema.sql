DROP TABLE IF EXISTS persons;
DROP TABLE IF EXISTS medicalrecords;
DROP TABLE IF EXISTS firestations;

CREATE TABLE firestations(
    stationNumber INT PRIMARY KEY AUTO_INCREMENT NOT NULL
);

CREATE TABLE medicalrecords(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    medications VARCHAR(255) NOT NULL,
    allergies VARCHAR(255) NOT NULL
);

CREATE TABLE persons(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    firstName VARCHAR(50) NOT NULL,
    lastName VARCHAR(50) NOT NULL,
    birthDate CHAR(10) NOT NULL,
    address VARCHAR(100) NOT NULL,
    city VARCHAR(40) NOT NULL,
    zip CHAR(5) NOT NULL,
    phone CHAR(12) NOT NULL,
    email VARCHAR(60) NOT NULL,
    firestation_id INT,
    medicalrecord_id INT,
    UNIQUE(medicalrecord_id),
    CONSTRAINT fullName UNIQUE (firstName,lastName),
    FOREIGN KEY(firestation_id) REFERENCES firestations(stationNumber) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY(medicalrecord_id) REFERENCES medicalrecords(id) ON DELETE SET NULL ON UPDATE CASCADE
);