package com.safetynet.alerts.service;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.repository.MedicalRecordsRepository;

@Service
public class MedicalRecordService {
    @Autowired
    private MedicalRecordsRepository medicalRecordsRepository;
    @Autowired
    private PersonService personService;

    public Iterable<MedicalRecord> getMedicalRecords() {
        return medicalRecordsRepository.findAll();
    }

    public Optional<MedicalRecord> getMedicalRecord(int id) {
        Optional<MedicalRecord> medicalRecord = medicalRecordsRepository.findById(id);
        try {
            medicalRecord.get();
            return medicalRecord;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The specified person does not exist.");
        }
    }

    public Optional<MedicalRecord> getMedicalRecordByPersonId(int personId) {
        Optional<MedicalRecord> record = null;
        Optional<Person> person = personService.getPerson(personId);

        person.get();
            
        record = Optional.ofNullable(person.get().getMedicalRecord());

        try {
            record.get();
            return record;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The specified person don't have medical records.");
        }
    }

    public Iterable<MedicalRecord> deleteMedicalRecord(Map<String, String> firstNameAndLastName) {

        if(firstNameAndLastName.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Two parameters should be here.");
        }

        Person personToUpdate = personService.getPersonByLastNameAndFirstName(firstNameAndLastName).get();

        MedicalRecord m = personToUpdate.getMedicalRecord();

        if (m != null) {
            medicalRecordsRepository.deleteById(m.getId());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This person don't have any medical records to delete.");
        }

        return getMedicalRecords();
    }

    public MedicalRecord saveMedicalRecord(MedicalRecord medicalRecord) {
        if (medicalRecord.getMedications() == null || medicalRecord.getAllergies() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Both medications and allergies must not be null.");
        }
        return medicalRecordsRepository.save(medicalRecord);
    }

    public MedicalRecord update(Integer id, MedicalRecord medicalRecordWithNewData) {
        Optional<MedicalRecord> m = getMedicalRecord(id);
        MedicalRecord currentMedicalRecord = null;
        
        if (m.isPresent()) {
            currentMedicalRecord = m.get();

            Person person = medicalRecordWithNewData.getPerson();
            String[] medications = medicalRecordWithNewData.getMedications();
            String[] allergies = medicalRecordWithNewData.getAllergies();

            if (medications != null) {
                currentMedicalRecord.setMedications(medications);
            }
            if (allergies != null) {
                currentMedicalRecord.setAllergies(allergies);
            }
            if (person != null) {
                currentMedicalRecord.setPerson(person);
            }

            saveMedicalRecord(currentMedicalRecord);
        }
        return currentMedicalRecord;
    }

    public void medicalRecordToPersonMapping(int person_id, int medicalRecord_id) {
        Optional<MedicalRecord> m = getMedicalRecord(medicalRecord_id);
        Optional<Person> p = personService.getPerson(person_id);

        if (m.isPresent() && p.isPresent()) {
            MedicalRecord medicalRecord = m.get();
            Person person = p.get();

            if (person.getMedicalRecord() == null || person.getMedicalRecord().getId() == 0) {
                
                person.setMedicalRecord(medicalRecord);
                personService.updatePerson(person.getId(), person);

                medicalRecord.setPerson(person);
                update(medicalRecord.getId(), medicalRecord);

            } else {
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "The specified person already have a medical record.");
            }
        }
    }
}
