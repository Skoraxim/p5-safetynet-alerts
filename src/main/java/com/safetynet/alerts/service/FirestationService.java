package com.safetynet.alerts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.model.CustomEndpointsObjects.FirestationWithPersonDetailsForGetMappingStationNumber;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonsDetailsForSpecifiedStations;
import com.safetynet.alerts.repository.FirestationsRepository;
import com.safetynet.alerts.utils.ListSort;

@Service
public class FirestationService {    
    @Autowired
    private FirestationsRepository firestationsRepository;
    @Autowired
    private PersonService personService;

    public Iterable<Firestation> getFirestations() {
        return firestationsRepository.findAll();
    }

    public Optional<Firestation> getFirestation(final int id) {
        Optional<Firestation> firestation = firestationsRepository.findById(id);
        try {
            firestation.get();
            return firestation;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The specified firestation does not exist.");
        }
    }

    public Firestation saveFirestation(Firestation firestation) {
        Firestation savedFirestation = firestationsRepository.save(firestation);
        return savedFirestation;
    }

    public void createFirestationToPersonMapping(int firestation_id, int person_id) {
        Optional<Person> p = personService.getPerson(person_id);
        Optional<Firestation> f = getFirestation(firestation_id);

        if (p.isPresent() && f.isPresent()) {
            if (p.get().getFirestation() == f.get() || p.get().getFirestation() == null) {
                updateFirestationToPersonMapping(firestation_id, person_id);
            } else {
                throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "The specified person already have an other firestation mapped to it.");
            }
        }
    }

    public Firestation updateFirestation(int id, Firestation firestationWithNewData) {
        Optional<Firestation> f = getFirestation(id);
        Firestation firestation = f.get();

        List<Person> persons = firestationWithNewData.getPersons();

        if (persons != null) {
            firestation.setPersons(persons);
        }

        return saveFirestation(firestation);
    }

    public boolean deleteFirestation(int id) {
        Optional<Firestation> f = getFirestation(id);

        Firestation firestationToDelete = f.get();

        if (firestationToDelete.getPersons() == null || firestationToDelete.getPersons().size() == 0) {
            firestationsRepository.deleteById(id);
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "A firestation can't be deleted if it covers at least one person.");
        }
    }

    public void updateFirestationToPersonMapping(Integer newFirestation_id, int person_id) {
        Optional<Person> p = personService.getPerson(person_id);
        Person currentPerson = null;
        Firestation firestation = null;

        if(p.isPresent() && newFirestation_id == null) {
            currentPerson = p.get();

            currentPerson.setFirestation(null);

            personService.updatePerson(currentPerson.getId(), currentPerson);

        } else if(p.isPresent() && newFirestation_id != null) {
            Optional<Firestation> f = getFirestation(newFirestation_id);

            if (f.isPresent()) {

                currentPerson = p.get();
                firestation = f.get();

                currentPerson.setFirestation(firestation);
                
                personService.updatePerson(currentPerson.getId(), currentPerson);

                List<Person> firestationPersonsList = firestation.getPersons();

                if (firestationPersonsList == null) {
                    firestationPersonsList = new ArrayList<>();
                }

                firestationPersonsList.add(currentPerson);

                firestation.setPersons(firestationPersonsList);
                
                updateFirestation(firestation.getId(), firestation);

            }
        }
    }

    public FirestationWithPersonDetailsForGetMappingStationNumber getPersonsDetailsByFirestation(int station_id) {
        Firestation firestation = getFirestation(station_id).get();
        List<Person> persons = firestation.getPersons();
        int adultCount = amountOfAdults(persons);
        int childCount = amountOfChilds(persons);

        return new FirestationWithPersonDetailsForGetMappingStationNumber(firestation, persons, childCount, adultCount);
    }

    public List<PersonsDetailsForSpecifiedStations> getPersonsDetailsForSpecifiedStations(List<Integer> stations) {
        List<PersonsDetailsForSpecifiedStations> personsDetails = new ArrayList<>();
        List<Person> persons = new ArrayList<>();

        for (Integer i : stations) {
            Optional<Firestation> f = getFirestation(i);
            Firestation firestation = f.get();

            for (Person person : firestation.getPersons()) {
                persons.add(person);
            }
        }

        persons = ListSort.personSortByAddress(persons);

        for (Person p : persons) {
            PersonsDetailsForSpecifiedStations person = new PersonsDetailsForSpecifiedStations(p.getAddress(), p.getZipCode(), p.getCity(), p.getFirstName(), p.getLastName(), p.getPhoneNumber(), p.getAge(), p.getMedicalRecord());
            personsDetails.add(person);
        }

        return personsDetails;
    }

    public List<String> getPhoneNumbersForAStation(int stationNumber) {
        Firestation f = getFirestation(stationNumber).get();
        List<Person> personsInFirestaiton = new ArrayList<>();
        List<String> phoneNumbers = null;

        personsInFirestaiton = f.getPersons();

        if (personsInFirestaiton.size() != 0) {
            phoneNumbers = new ArrayList<>();

            for (Person p : personsInFirestaiton) {
                if (!phoneNumbers.contains(p.getPhoneNumber())) {
                    phoneNumbers.add(p.getPhoneNumber());
                }
            }
        }

        return phoneNumbers;
    }




    private int amountOfAdults(List<Person> personsList) {
        int adultsCount = 0;

        for (Person person : personsList) {

            if (person.getAge() > 18) {
                adultsCount++;
            }
        }

        return adultsCount;
    }

    private int amountOfChilds(List<Person> personsList) {
        int childsCount = 0;

        for (Person person : personsList) {

            if (person.getAge() <= 18) {
                childsCount++;
            }
        }

        return childsCount;
    }

}
