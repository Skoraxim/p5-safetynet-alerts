package com.safetynet.alerts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.model.CustomEndpointsObjects.ChildListWithOtherMembers;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonWithLimitedDetails;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonsDetailsByAddressForGetMappingFire;
import com.safetynet.alerts.repository.PersonsRepository;
import com.safetynet.alerts.utils.ListSort;

@Service
public class PersonService {
    @Autowired
    private PersonsRepository personsRepository;

    public Iterable<Person> getPersons() {
        return personsRepository.findAll();
    }

    public Optional<Person> getPerson(final int id) {
        Optional<Person> person = personsRepository.findById(id);
        try {
            person.get();
            return person;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The specified person does not exist.");
        }
    }

    public Optional<Person> getPersonByLastNameAndFirstName(Map<String,String> firstNameAndLastName) {
        if (firstNameAndLastName.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only two Strings should be here.");
        }

        Optional<Person> p = personsRepository.findByLastNameAndFirstName(firstNameAndLastName.get("lastName"), firstNameAndLastName.get("firstName"));
        try {
            p.get();
            return p;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The specified person does not exist.");
        }
    }

    public Person savePerson(Person person) {
        checkPersonDetails(person);

        Person savedPerson = personsRepository.save(person);
        return savedPerson;
    }

    public void deletePerson(Map<String, String> firstNameAndLastName) {

        if (firstNameAndLastName.size() != 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only two Strings should be here.");
        }

        Person p = getPersonByLastNameAndFirstName(firstNameAndLastName).get();

        personsRepository.deleteById(p.getId());
    }

    public Person updatePerson(int id, Person personWithNewData) {
        Optional<Person> p = getPerson(id);
        Person currentPerson = null;
        
        if(p.isPresent()) {
            currentPerson = p.get();

            String firstName = personWithNewData.getFirstName();
            String lastName = personWithNewData.getLastName();
            String birthDate = personWithNewData.getBirthDate();
            String address = personWithNewData.getAddress();
            String city = personWithNewData.getCity();
            String zipCode = personWithNewData.getZipCode();
            String phoneNumber = personWithNewData.getPhoneNumber();
            String email = personWithNewData.getEmail();
            Firestation firestation = personWithNewData.getFirestation();
            MedicalRecord medicalRecord = personWithNewData.getMedicalRecord();

            if(firstName != null) {
                currentPerson.setFirstName(firstName);
            }
            if(lastName != null) {
                currentPerson.setLastName(lastName);
            }
            if (birthDate != null) {
                currentPerson.setBirthDate(birthDate);
            }
            if (address != null) {
                currentPerson.setAddress(address);
            }
            if (city != null) {
                currentPerson.setCity(city);
            }
            if (zipCode != null) {
                currentPerson.setCity(city);
            }
            if (phoneNumber != null) {
                currentPerson.setPhoneNumber(phoneNumber);
            }
            if (email != null) {
                currentPerson.setEmail(email);
            }
            if (firestation != null) {
                currentPerson.setFirestation(firestation);
            }
            if (medicalRecord != null) {
                currentPerson.setMedicalRecord(medicalRecord);
            }

            savePerson(currentPerson);
        }
        return currentPerson;
    }

    public Person setAllFieldsAsNull(Person personToNull) {
        Person p = new Person();
        p.setId(personToNull.getId());
        
        return p;
    }

    public List<String> getAllMailsByCity(String city) {
        Iterable<Person> persons = personsRepository.findByCity(city);
        if (((List<Person>) persons).size() == 0) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "0 persons where found for this city, maybe it does not exist?");
        }
        List<String> emails = new ArrayList<>();

        for (Person person : persons) {
            emails.add(person.getEmail());
        }

        return emails;
    }

    public PersonWithLimitedDetails getPersonDetailsLimited(Map<String, String> firstNameAndLastName) {
        PersonWithLimitedDetails personLimited = null;
        Person person = getPersonByLastNameAndFirstName(firstNameAndLastName).get();

        personLimited = new PersonWithLimitedDetails(person);

        return personLimited;
    }

    public List<PersonsDetailsByAddressForGetMappingFire> getPersonsDetailsForSpecifiedAddress(String address) {
        List<Person> basePersons = new ArrayList<>();
        List<PersonsDetailsByAddressForGetMappingFire> personsDetailed = new ArrayList<>();

        basePersons = (List<Person>) personsRepository.findByAddress(address);

        if(basePersons.size() == 0) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This address is not found or nobody lives here.");
        }

        for (Person person : basePersons) {
            PersonsDetailsByAddressForGetMappingFire p = new PersonsDetailsByAddressForGetMappingFire(person);

            personsDetailed.add(p);
        }

        return personsDetailed;
    }

    public ChildListWithOtherMembers getChildsAndOthersMembersByAddress(String address) {
        List<Person> persons = (List<Person>) personsRepository.findByAddress(address);
        List<Person> personsToAdd = null;
        ChildListWithOtherMembers childList = null;
        boolean atLeastOneChild = false;

        if (persons.size() != 0) {
            for (Person p : persons) {
                if (p.getAge() <= 18) {
                    atLeastOneChild = true;
                }
            }

            if (atLeastOneChild) {
                personsToAdd = new ArrayList<>();

                for (Person p : persons) {
                    personsToAdd.add(p);
                }
            }

            personsToAdd = ListSort.personSortByAge(personsToAdd);

            if (personsToAdd != null) {
                childList = new ChildListWithOtherMembers(personsToAdd);
            }

        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This address is not found or nobody lives here.");
        }

        return childList;
    }





    private boolean checkPersonDetails(Person p) {
        int nullCounter = 0;
        if (p.getFirstName() == null || p.getFirstName() == "") {
            nullCounter++;
        }
        if (p.getLastName() == null || p.getLastName() == "") {
            nullCounter++;
        }
        if (p.getBirthDate() == null || p.getBirthDate() == "") {
            nullCounter++;
        }
        if (p.getAddress() == null || p.getAddress() == "") {
            nullCounter++;
        }
        if (p.getCity() == null|| p.getCity() == "") {
            nullCounter++;
        }
        if (p.getZipCode() == null || p.getZipCode() == "") {
            nullCounter++;
        }
        if (p.getPhoneNumber() == null || p.getPhoneNumber().length() != 12) {
            nullCounter++;
        }
        if (p.getEmail() == null || p.getEmail() == "") {
            nullCounter++;
        }

        if (nullCounter > 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, nullCounter + " mandatory fields are null for this person.");
        }

        return true;
    }
}
