package com.safetynet.alerts.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestLogger {
    private static final Logger log = LoggerFactory.getLogger(RequestLogger.class);

    public static void logRequest(HttpServletRequest request) {
        log.info("Request URL : " + request.getRequestURL());
        log.info("Request Method : " + request.getMethod());
        
        try {
            log.info("Request Body : " + readInputStream(request.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String readInputStream(InputStream inputStream) {
        String result = new String();

        int bufferSize = 1024;
        char[] buffer = new char[bufferSize];
        StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        
        try {
            for (int numRead; (numRead = in.read(buffer, 0, buffer.length)) > 0; ) {
                out.append(buffer, 0, numRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        result = out.toString();

        if (result.equals("")) {
            result = "No Body";
        }

        return result;

    }
}
