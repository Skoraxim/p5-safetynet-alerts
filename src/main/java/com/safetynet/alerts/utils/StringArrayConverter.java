package com.safetynet.alerts.utils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.ArrayNode;

@Converter
public class StringArrayConverter implements AttributeConverter<String[], String> 
{
    private static final Logger log = LoggerFactory.getLogger(StringArrayConverter.class);

    @Override
    public String convertToDatabaseColumn(String[] attribute)
    {
        if (attribute != null && attribute.length != 0) {
            String result = "";

            if(attribute.length >= 0) {
                for(String str : attribute) {
                    result += str + ",";
                }

            return result.subSequence(0, result.length()-1).toString();
            }
        }

        log.warn("Trying to convert from String array to Database String, but array is empty.");

        return new String();
    }

    @Override
    public String[] convertToEntityAttribute(String dbData)
    {
        String[] result = {""};

        if(dbData != null && dbData != "") {
            result = dbData.split(",");
            return result;
        }
        log.warn("Trying to convert from Database String to String array, but column is empty.");

        return result;
    }

    public String[] convertToStringArray(ArrayNode arrayNode) {
        String stringToConvert = arrayNode.toString();
        
        String[] allStrings = StringUtils.substringsBetween(stringToConvert, "\"", "\"");
        
        return allStrings;
    }
}