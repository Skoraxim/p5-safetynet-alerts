package com.safetynet.alerts.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;

public class ListSort {

    public static List<Person> personSortByAddress(List<Person> listOfPersonsToSort) {
        List<Person> persons = new ArrayList<>();

        for (Person person : listOfPersonsToSort) {
            persons.add(person);
        }

        Collections.sort(persons, new Comparator<Person>() {
            public int compare(Person p1, Person p2) {
                return p1.getAddress().compareTo(p2.getAddress());
            }
        });

        return persons;
    }

    public static List<Person> personSortByAge(List<Person> listOfPersonsToSort) {
        if (listOfPersonsToSort == null) {
            return null;
        }
        
        List<Person> persons = new ArrayList<>();

        for (Person person : listOfPersonsToSort) {
            persons.add(person);
        }

        Collections.sort(persons, new Comparator<Person>() {
            public int compare(Person p1, Person p2) {
                return p1.getAge().compareTo(p2.getAge());
            }
        });

        return persons;
    }

    public static List<Firestation> firestationSortByNumber(List<Firestation> listOfFirestationsToSort) {
        List<Firestation> firestations = new ArrayList<>();
        
        for (Firestation firestation : listOfFirestationsToSort) {
            firestations.add(firestation);
        }

        Collections.sort(firestations, new Comparator<Firestation>() {
            public int compare(Firestation f1, Firestation f2) {
                Integer num1 = f1.getId();
                Integer num2 = f2.getId();

                return num1.compareTo(num2);
            }
        });

        return firestations;
    }
}
