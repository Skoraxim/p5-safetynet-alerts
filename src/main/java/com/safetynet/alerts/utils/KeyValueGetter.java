package com.safetynet.alerts.utils;

import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;

public class KeyValueGetter {

    public static <T, E> T getKeyByValue(Map<T,E> mapToSearch, E value) {
        for (Entry<T, E> entry : mapToSearch.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
