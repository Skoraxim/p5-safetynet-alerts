package com.safetynet.alerts.utils;

import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StringToDateConverter {
    public static final Logger log = LoggerFactory.getLogger(StringToDateConverter.class);

    /**
     * Convert a String to a java.sql.Date.
     * Format is MM/DD/YYYY.
     * @param dateToConvert
     */
    public Date convertToDate(String dateToConvert) {
        try {
            if(DateFormatValidator.isValid(dateToConvert)) {
                return new Date(new SimpleDateFormat("MM/dd/yyyy").parse(dateToConvert).getTime());
            } else {}
        } catch (ParseException e) {
            log.error("Unable to parse string to date.");
        }
        return null;
    }
}
