package com.safetynet.alerts.utils;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateFormatValidator {
    private static final Logger log = LoggerFactory.getLogger(DateFormatValidator.class);

    public static boolean isValid(final String dateToValidate) throws ParseException {
        boolean valid = false;

        try {
            LocalDate.parse(dateToValidate, DateTimeFormatter.ofPattern("MM/dd/uuuu").withResolverStyle(ResolverStyle.STRICT));
            valid = true;
        } catch (DateTimeParseException e) {
            log.error("Error while checking if date is in a valid format.");
            throw new ParseException(e.getMessage(), e.getErrorIndex());
        }
        return valid;
    }
}
