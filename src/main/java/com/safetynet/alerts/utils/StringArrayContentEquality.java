package com.safetynet.alerts.utils;

public class StringArrayContentEquality {
    public static boolean testContentEquality(String[] strArr1, String[] strArr2) {

        if(strArr1.length == strArr2.length) {
            for (int i = 0; i < strArr1.length; i++) {
                if(!strArr1[i].equals(strArr2[i])) {
                    return false;

                }
            }
            return true;

        }
        return false;

    }
}
