package com.safetynet.alerts.utils;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseLogger {
    private static final Logger log = LoggerFactory.getLogger(ResponseLogger.class);

    public static void logResponse(HttpServletResponse response) {
        log.info("Response Status Code : " + response.getStatus());
        log.info("Response Headers : " + response.getHeaderNames().toString());
    }
}
