package com.safetynet.alerts.databaseFiller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.service.PersonService;

@Component
public class FirstFillDatabase {
    private static final Logger log = LoggerFactory.getLogger(FirstFillDatabase.class);

    @Autowired
    private MedicalRecordService medicalRecordService;
    @Autowired
    private FirestationService firestationService;
    @Autowired
    private PersonService personService;

    private ListToPostGenerator listToPostGenerator = new ListToPostGenerator();

    private List<Firestation> firestations = null;
    private List<MedicalRecord> medicalrecords = null;
    private List<Person> persons = null;

    public void generateData() {
        firestations = listToPostGenerator.createFirestationList();
        medicalrecords = listToPostGenerator.createMedicalrecordList();
        
        try {
            persons = listToPostGenerator.createPersonList();
        } catch (Exception e) {
            log.error("createFirestationList and createMedicalRecord should be called before createPersonList.");
            persons = null;
        }

        if(persons != null){
            saveDataToDatabase();
            mapPersonsToFirestations();
            mapPersonsToMedicalRecord();
        }
    }

    private void saveDataToDatabase() {
        if(firestations != null && !firestations.isEmpty()) {
            for (Firestation firestation : firestations) {
                firestationService.saveFirestation(firestation);
            }
        }

        if(medicalrecords != null && !medicalrecords.isEmpty()) {
            for (MedicalRecord medicalRecord : medicalrecords) {
                medicalRecordService.saveMedicalRecord(medicalRecord);
            }
        }
        
        if(persons != null && !persons.isEmpty()) {
            for (Person person : persons) {
                personService.savePerson(person);
            }   
        }
    }

    private void mapPersonsToFirestations() {
        Map<String, Firestation> stationToAddressLink = listToPostGenerator.getFirestationToAdressLink();

        List<Person> persons = (List<Person>) personService.getPersons();
        List<Firestation> firestations = (List<Firestation>) firestationService.getFirestations();

        for (Person p : persons) {
            for (Firestation f : firestations) {

                //This checks if it's not a value from another test, and in this case, we skip.
                if(!p.getAddress().toLowerCase().contains("paris") && !p.getAddress().toLowerCase().contains("potiers")) {

                    if (f.getId() == stationToAddressLink.get(p.getAddress()).getId()) {
                        firestationService.createFirestationToPersonMapping(f.getId(), p.getId());
                        break;
                    }
                }

                
            }
        }
    }

    private void mapPersonsToMedicalRecord() {
        Map<Integer, String> medicalrecordIdToNameLink = listToPostGenerator.getMedicalrecordIdToNameLink();

        List<Person> persons = (List<Person>) personService.getPersons();
        List<MedicalRecord> medicalRecords = (List<MedicalRecord>) medicalRecordService.getMedicalRecords();

        String fullName = null;

        for (Person p : persons) {
            fullName = p.getFirstName() + "_" + p.getLastName();
            for (MedicalRecord m : medicalRecords) {
                Integer index = m.getId();

                if (fullName.equals(medicalrecordIdToNameLink.get(index))) {
                    medicalRecordService.medicalRecordToPersonMapping(p.getId(), m.getId());
                    break;
                }
            }
        }
    }



    

    public boolean isDataPresentInDatabase() {
        ArrayList<Person> personPresence = (ArrayList<Person>) personService.getPersons();
        ArrayList<Firestation> firestationPresence = (ArrayList<Firestation>) firestationService.getFirestations();
        ArrayList<MedicalRecord> medicalRecordPresence = (ArrayList<MedicalRecord>) medicalRecordService.getMedicalRecords();

        if (personPresence.size() >= 1 || firestationPresence.size() >= 1 || medicalRecordPresence.size() >= 1) {
            return true;
        } else {
            return false;
        }
    }
}