package com.safetynet.alerts.databaseFiller;

import java.io.File;

import com.safetynet.alerts.constants.FilePath;

public class JsonFileGetter {

    public File getPersonJson() {
        return new File(FilePath.JSON_FILE_PATH_PERSON);
    }

    public File getFirestationJson() {
        return new File(FilePath.JSON_FILE_PATH_FIRESTATION);
    }

    public File getMedicalrecordJson() {
        return new File(FilePath.JSON_FILE_PATH_MEDICALRECORD);
    }

}
