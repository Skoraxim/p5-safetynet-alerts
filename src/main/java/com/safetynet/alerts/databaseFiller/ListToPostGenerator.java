package com.safetynet.alerts.databaseFiller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.utils.KeyValueGetter;
import com.safetynet.alerts.utils.ListSort;
import com.safetynet.alerts.utils.StringArrayConverter;

public class ListToPostGenerator {
    private static final Logger log = LoggerFactory.getLogger(ListToPostGenerator.class);

    private ObjectMapper mapper = new ObjectMapper();

    private JsonFileGetter jsonFileGetter = new JsonFileGetter();
    private StringArrayConverter stringArrayConverter = new StringArrayConverter();

    private List<Firestation> firestationToPost = new ArrayList<Firestation>();
    private List<MedicalRecord> medicalrecordToPost = new ArrayList<MedicalRecord>();
    private List<Person> personToPost = new ArrayList<Person>();

    public Map<String, Firestation> firestationToAdressLink = new HashMap<>();
    public Map<Integer, String> medicalrecordIdToNameLink = new HashMap<>();
    public Map<Integer, String> medicalrecordIdToBirthDateLink = new HashMap<>();

    private boolean createRecordsListHaveBeenCalled = false;
    private boolean createStationsListHaveBeenCalled = false;
    
    public List<Firestation> createFirestationList() {
        List<Firestation> alreadySeenStation = new ArrayList<>();
        Firestation currentFirestation = new Firestation();
        int stationId = 1;

        try {
            ArrayNode rootArrayFirestation = (ArrayNode)mapper.readTree(jsonFileGetter.getFirestationJson());

            for(JsonNode root : rootArrayFirestation) {
                stationId = root.path("station").asInt();
                currentFirestation.setId(stationId);

                if(!alreadySeenStation.contains(currentFirestation)) {
                    alreadySeenStation.add(currentFirestation);
                    firestationToPost.add(currentFirestation);
                }

                firestationToAdressLink.put(root.path("address").asText(), currentFirestation);

                currentFirestation = new Firestation();
            }

            stationId = 1;
            createStationsListHaveBeenCalled = true;

            firestationToPost = ListSort.firestationSortByNumber(firestationToPost);

            return firestationToPost;
        } catch (IOException e) {
            log.error("Unable to open firestations json file.");
        }

        return null;
    }
    
    public List<MedicalRecord> createMedicalrecordList() {
        MedicalRecord currentMedicalRecord = new MedicalRecord();
        String[] medications = null;
        String[] allergies = null;
        String currentFullName = null;
        String currentBirthDate = null;
        int manualIdValue = 1;

        try {
            ArrayNode rootArrayMedicalrecord = (ArrayNode)mapper.readTree(jsonFileGetter.getMedicalrecordJson());

            for (JsonNode root : rootArrayMedicalrecord) {
                currentMedicalRecord.setId(manualIdValue);

                medications = stringArrayConverter.convertToStringArray((ArrayNode)root.path("medications"));
                if (medications == null) {
                    medications = new String[] {""};
                }
                currentMedicalRecord.setMedications(medications);

                allergies = stringArrayConverter.convertToStringArray((ArrayNode)root.path("allergies"));
                if (allergies == null) {
                    allergies = new String[] {""};
                }
                currentMedicalRecord.setAllergies(allergies);
                
                currentFullName = root.path("firstName").asText() + "_" + root.path("lastName").asText();
                currentBirthDate = root.path("birthdate").asText();

                medicalrecordToPost.add(currentMedicalRecord);

                medicalrecordIdToNameLink.put(currentMedicalRecord.getId(), currentFullName);
                medicalrecordIdToBirthDateLink.put(currentMedicalRecord.getId(), currentBirthDate);

                manualIdValue++;
                currentFullName = null;
                currentBirthDate = null;
                medications = null;
                allergies = null;
                currentMedicalRecord = new MedicalRecord();
            }

            manualIdValue = 1;
            createRecordsListHaveBeenCalled = true;

            return medicalrecordToPost;
        } catch (IOException e) {
            log.error("Unable to open medical records json file.");
        }

        return null;
    }

    public List<Person> createPersonList() throws Exception {
        Person currentPerson = new Person();

        if (createRecordsListHaveBeenCalled && createStationsListHaveBeenCalled) {
            
        } else {
            throw new Exception();
        }
        
        try {
            ArrayNode rootArrayPerson = (ArrayNode)mapper.readTree(jsonFileGetter.getPersonJson());

            for (JsonNode root : rootArrayPerson) {
                //Firstly, set all the data we have directly in the person.json file
                currentPerson.setFirstName(root.path("firstName").asText());
                currentPerson.setLastName(root.path("lastName").asText());
                currentPerson.setAddress(root.path("address").asText());
                currentPerson.setCity(root.path("city").asText());
                currentPerson.setZipCode(root.path("zip").asText());
                currentPerson.setPhoneNumber(root.path("phone").asText());
                currentPerson.setEmail(root.path("email").asText());

                //Then, set all the data we previously got from others jsons files
                currentPerson.setId(KeyValueGetter.getKeyByValue(medicalrecordIdToNameLink, currentPerson.getFirstName() + "_" + currentPerson.getLastName()));
                currentPerson.setBirthDate(medicalrecordIdToBirthDateLink.get(currentPerson.getId()));

                personToPost.add(currentPerson);
                currentPerson = new Person();

            }

            return personToPost;
        } catch (IOException e) {
            log.error("Unable to open persons json file.");
        }
        return null;
    }





    public Map<String, Firestation> getFirestationToAdressLink() {
        Map<String, Firestation> map = new HashMap<>();
        map = firestationToAdressLink;

        return map;
    }

    public Map<Integer, String> getMedicalrecordIdToNameLink() {
        Map<Integer, String> map = new HashMap<>();
        map = medicalrecordIdToNameLink;

        return map;
    }

}
