package com.safetynet.alerts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.safetynet.alerts.databaseFiller.FirstFillDatabase;

@SpringBootApplication
public class App implements CommandLineRunner {
	public static final Logger log = LoggerFactory.getLogger(App.class);

	@Autowired
	private FirstFillDatabase databaseFiller;

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Override
	public void run(String...args) throws Exception {
		if(!databaseFiller.isDataPresentInDatabase()) {
			log.warn("No data in database, generating base data");

			databaseFiller.generateData();

		} else {
			log.info("Data found in database, skipping first initialization of data");
		}
	}

}
