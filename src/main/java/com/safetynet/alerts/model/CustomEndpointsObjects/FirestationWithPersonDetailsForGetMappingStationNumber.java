package com.safetynet.alerts.model.CustomEndpointsObjects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.utils.annotations.ExcludeConstructorFromJacocoGeneratedReport;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FirestationWithPersonDetailsForGetMappingStationNumber {
    @JsonIgnoreProperties({"persons"})
    private Firestation firestation;
    private int childCount;
    private int adultCount;

    @JsonIgnoreProperties({"birthDate", "email", "firestation", "medicalRecord", "age"})
    private List<Person> persons;

    @ExcludeConstructorFromJacocoGeneratedReport
    public FirestationWithPersonDetailsForGetMappingStationNumber(Firestation firestation, List<Person> persons, int childCount, int adultCount) {
        this.firestation = firestation;
        this.persons = persons;
        this.childCount = childCount;
        this.adultCount = adultCount;
    }
}
