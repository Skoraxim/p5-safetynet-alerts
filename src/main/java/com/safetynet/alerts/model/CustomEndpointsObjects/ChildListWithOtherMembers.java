package com.safetynet.alerts.model.CustomEndpointsObjects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.utils.annotations.ExcludeConstructorFromJacocoGeneratedReport;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChildListWithOtherMembers {

    @JsonIgnoreProperties({"birthDate", "address", "zipCode", "city", "phoneNumber", "email", "firestation", "medicalRecord"})
    private List<Person> persons;
    
    @ExcludeConstructorFromJacocoGeneratedReport
    public ChildListWithOtherMembers(List<Person> persons) {
        this.persons = persons;
    }
}
