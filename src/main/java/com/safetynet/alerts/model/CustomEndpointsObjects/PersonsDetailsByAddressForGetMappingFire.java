package com.safetynet.alerts.model.CustomEndpointsObjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.utils.annotations.ExcludeConstructorFromJacocoGeneratedReport;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonsDetailsByAddressForGetMappingFire {
    
    @JsonIgnoreProperties({"birthDate", "address", "city", "zipCode", "email"})
    private Person person;

    @ExcludeConstructorFromJacocoGeneratedReport
    public PersonsDetailsByAddressForGetMappingFire(Person person) {
        this.person = person;
    }
}
