package com.safetynet.alerts.model.CustomEndpointsObjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.utils.annotations.ExcludeConstructorFromJacocoGeneratedReport;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonWithLimitedDetails {

    @JsonIgnoreProperties({"birthDate", "phoneNumber", "firestation"})
    private Person person;

    @ExcludeConstructorFromJacocoGeneratedReport
    public PersonWithLimitedDetails(Person person) {
        this.person = person;
    }
}
