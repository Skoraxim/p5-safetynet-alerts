package com.safetynet.alerts.model.CustomEndpointsObjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.utils.annotations.ExcludeConstructorFromJacocoGeneratedReport;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PersonsDetailsForSpecifiedStations {
    private String address;
    private String zipCode;
    private String city;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private int age;
    
    @JsonIgnoreProperties({"person"})
    private MedicalRecord medicalRecord;

    @ExcludeConstructorFromJacocoGeneratedReport
    public PersonsDetailsForSpecifiedStations(String address, String zipCode, String city, String firstName, String lastName, String phoneNumber, int age, MedicalRecord medicalRecord) {
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.age = age;
        this.medicalRecord = medicalRecord;
    }
}
