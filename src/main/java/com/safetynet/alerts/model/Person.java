package com.safetynet.alerts.model;

import java.sql.Date;
import java.time.Period;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.utils.StringToDateConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
@Entity
@Table(name = "persons")
public class Person {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "birthDate")
    private String birthDate;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "zip")
    private String zipCode;

    @Column(name = "phone")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @ManyToOne(
        cascade = CascadeType.DETACH
    )
    @JoinColumn(name = "firestation_id")
    @JsonIgnoreProperties({"persons"})
    private Firestation firestation;

    @OneToOne(
        cascade = CascadeType.REMOVE
    )
    @JoinColumn(
        name = "medicalrecord_id",
        referencedColumnName = "id"
    )
    @JsonIgnoreProperties({"person"})
    private MedicalRecord medicalRecord;

    public Integer getAge() {
        StringToDateConverter converter = new StringToDateConverter();
        if(this.birthDate != null) {
            return Period.between(converter.convertToDate(this.birthDate).toLocalDate(), new Date(System.currentTimeMillis()).toLocalDate()).getYears();
        }
        return 0;
    }
}
