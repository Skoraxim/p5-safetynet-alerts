package com.safetynet.alerts.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.safetynet.alerts.utils.StringArrayConverter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
@Entity
@Table(name = "medicalrecords")
public class MedicalRecord {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(
        mappedBy = "medicalRecord",
        cascade = CascadeType.DETACH
    )
    @JsonIgnoreProperties({"medicalRecord"})
    private Person person;

    @Column(name = "medications")
    @Convert(converter = StringArrayConverter.class)
    private String[] medications;

    @Column(name = "allergies")
    @Convert(converter = StringArrayConverter.class)
    private String[] allergies;
}
