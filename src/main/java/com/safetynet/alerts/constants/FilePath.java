package com.safetynet.alerts.constants;

public class FilePath {
    public static final String JSON_FILE_PATH_PERSON = "src/main/resources/person_data.json";
    public static final String JSON_FILE_PATH_FIRESTATION = "src/main/resources/firestation_data.json";
    public static final String JSON_FILE_PATH_MEDICALRECORD = "src/main/resources/medicalrecord_data.json";
}
