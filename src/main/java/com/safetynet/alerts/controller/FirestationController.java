package com.safetynet.alerts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.CustomEndpointsObjects.FirestationWithPersonDetailsForGetMappingStationNumber;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.utils.RequestLogger;
import com.safetynet.alerts.utils.ResponseLogger;

@RestController
public class FirestationController {
    @Autowired
    private FirestationService firestationService;

    @PostMapping("/firestation")
    public Firestation createFirestation(@RequestBody Firestation firestation) {
        Firestation fs = firestationService.saveFirestation(firestation);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return fs;
    }

    @PostMapping("/firestation/{firestation_id}/{person_id}")
    public void createFirestationToPersonMapping(@PathVariable("firestation_id") final int firestation_id, @PathVariable("person_id") final int person_id) {
        firestationService.createFirestationToPersonMapping(firestation_id, person_id);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    @GetMapping("/firestation")
    public Iterable<Firestation> getAll() {
        List<Firestation> fs = (List<Firestation>) firestationService.getFirestations();
        
        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return fs;
    }

    @GetMapping("/firestation/{id}")
    public Firestation getById(@PathVariable("id") final int id) {
        Firestation fs = firestationService.getFirestation(id).get();

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
        
        return fs;
    }

    @PutMapping("/firestation/{newFirestation_id}/{person_id}")
    public void updateFirestationToPersonMapping(@PathVariable("newFirestation_id") final Integer newFirestation_id, @PathVariable("person_id") final int person_id) {
        firestationService.updateFirestationToPersonMapping(newFirestation_id, person_id);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    @DeleteMapping("/firestation/{id}")
    public void delete(@PathVariable("id") final int id) {
        firestationService.deleteFirestation(id);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    @DeleteMapping("/firestation/{firestation_id}/{person_id}")
    public void deleteFirestationToPersonMapping(@PathVariable("firestation_id") final int id, @PathVariable("person_id") final int person_id) {
        updateFirestationToPersonMapping(null, person_id);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    /////ADDITIONAL ENDPOINT/////

    @RequestMapping(value = "/firestation", params = "stationNumber")
    public FirestationWithPersonDetailsForGetMappingStationNumber getPersonDetailsByStationId(@RequestParam final int stationNumber) {
        FirestationWithPersonDetailsForGetMappingStationNumber personsDetailsAndFirestaiton = firestationService.getPersonsDetailsByFirestation(stationNumber);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return personsDetailsAndFirestaiton;
    }
}
