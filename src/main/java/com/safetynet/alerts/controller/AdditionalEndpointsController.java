package com.safetynet.alerts.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.safetynet.alerts.model.CustomEndpointsObjects.ChildListWithOtherMembers;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonWithLimitedDetails;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonsDetailsByAddressForGetMappingFire;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonsDetailsForSpecifiedStations;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.PersonService;
import com.safetynet.alerts.utils.RequestLogger;
import com.safetynet.alerts.utils.ResponseLogger;

@RestController
public class AdditionalEndpointsController {
    
    @Autowired
    private FirestationService firestationService;
    @Autowired
    private PersonService personService;

    @GetMapping("/childAlert")
    public ChildListWithOtherMembers getChildsAndOthersMembersByAddress(@RequestParam String address) {
        ChildListWithOtherMembers childList = personService.getChildsAndOthersMembersByAddress(address);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
        
        return childList;
    }

    @GetMapping("/phoneAlert")
    public List<String> getPhoneNumbersForAFirestation(@RequestParam int firestation) {
        List<String> phoneNumbers = firestationService.getPhoneNumbersForAStation(firestation);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return phoneNumbers;
    }

    @GetMapping("/fire")
    public List<PersonsDetailsByAddressForGetMappingFire> getFirestationWithPersonsDetails(@RequestParam String address) {
        List<PersonsDetailsByAddressForGetMappingFire> personsDetails = personService.getPersonsDetailsForSpecifiedAddress(address);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return personsDetails;
    }

    @GetMapping("/flood/stations")
    public List<PersonsDetailsForSpecifiedStations> getPersonsDetailsForSpecifiedStations(@RequestParam List<Integer> stations) {
        List<PersonsDetailsForSpecifiedStations> personsDetails = firestationService.getPersonsDetailsForSpecifiedStations(stations);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return personsDetails;
    }

    @GetMapping("/personInfo")
    public PersonWithLimitedDetails getPersonDetailsLimited(@RequestParam Map<String, String> firstNameAndLastName) {
        PersonWithLimitedDetails personWithLimitedDetails = personService.getPersonDetailsLimited(firstNameAndLastName);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return personWithLimitedDetails;
    }

    @GetMapping("/communityEmail")
    public List<String> getAllEmailsForACity(@RequestParam String city) {
        List<String> emails = personService.getAllMailsByCity(city);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return emails;
    }
}
