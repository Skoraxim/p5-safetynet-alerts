package com.safetynet.alerts.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.PersonService;
import com.safetynet.alerts.utils.RequestLogger;
import com.safetynet.alerts.utils.ResponseLogger;

@RestController
public class PersonController {
    
    @Autowired
    private PersonService personService;

    @PostMapping("/person")
    public Person create(@RequestBody Person person) {
        Person savedPerson = personService.savePerson(person);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return savedPerson;
    }

    @GetMapping("/person")
    public Iterable<Person> getAll() {
        List<Person> listPersons = (List<Person>) personService.getPersons();

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
        
        return listPersons;
    }

    @GetMapping("/person/{id}") 
    public Person getById(@PathVariable("id") final int id) {
        Person person = personService.getPerson(id).get();

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return person;
    }

    @PutMapping("/person/{id}")
    public void update(@PathVariable("id") final int id, @RequestBody Person personWithNewData) {
        personService.updatePerson(id, personWithNewData);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    @DeleteMapping("/person")
    public void deletePerson(@RequestParam Map<String,String> firstNameAndLastName) {
        personService.deletePerson(firstNameAndLastName);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }
}
