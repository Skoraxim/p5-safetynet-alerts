package com.safetynet.alerts.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.utils.RequestLogger;
import com.safetynet.alerts.utils.ResponseLogger;

@RestController
@RequestMapping("/medicalRecord")
public class MedicalRecordController {

    @Autowired
    private MedicalRecordService medicalRecordService;

    @PostMapping
    public MedicalRecord create(@RequestBody MedicalRecord medicalRecord) {
        MedicalRecord record = medicalRecordService.saveMedicalRecord(medicalRecord);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return record;
    }
    
    @PostMapping("/{person_id}/{medicalRecord_id}")
    public void mapPersonToMedicalRecord(@PathVariable("person_id") final int person_id, @PathVariable("medicalRecord_id") final int medicalRecord_id) {
        medicalRecordService.medicalRecordToPersonMapping(person_id, medicalRecord_id);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    @GetMapping
    public Iterable<MedicalRecord> getAll() {
        List<MedicalRecord> listOfRecords = (List<MedicalRecord>) medicalRecordService.getMedicalRecords();

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return listOfRecords;
    }

    @GetMapping("/{id}")
    public MedicalRecord getById(@PathVariable("id") final int id) {
        MedicalRecord record = medicalRecordService.getMedicalRecord(id).get();

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());

        return record;
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") final int id, @RequestBody MedicalRecord medicalRecordWithNewData) {
        medicalRecordService.update(id, medicalRecordWithNewData);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }

    @DeleteMapping
    public void delete(@RequestParam Map<String,String> firstNameAndLastName) {
        medicalRecordService.deleteMedicalRecord(firstNameAndLastName);

        RequestLogger.logRequest(((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest());
        ResponseLogger.logResponse(((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse());
    }
}
