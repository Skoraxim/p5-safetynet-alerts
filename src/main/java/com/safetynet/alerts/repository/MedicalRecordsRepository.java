package com.safetynet.alerts.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.safetynet.alerts.model.MedicalRecord;

@Repository
public interface MedicalRecordsRepository extends CrudRepository<MedicalRecord, Integer> {

    Optional<MedicalRecord> findByPersonId(int personId);
    
}
