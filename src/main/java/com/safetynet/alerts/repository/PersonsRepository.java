package com.safetynet.alerts.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.safetynet.alerts.model.Person;

@Repository
public interface PersonsRepository extends CrudRepository<Person, Integer> {
    
    public Optional<Person> findByLastNameAndFirstName(String lastName, String firstName);
    public Iterable<Person> findByCity(String city);
    public Iterable<Person> findByAddress(String address);
}
