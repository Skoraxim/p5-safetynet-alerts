package com.safetynet.alerts;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.safetynet.alerts.model.Person;

public class PersonGetAgeTest {
    
    @Test
    public void personAge_IsCalculatedCorrectly() {
        ///ARRANGE///

        int personAge = -1;

        Person p = new Person();
        p.setBirthDate("01/01/2000");

        ///ACT///

        personAge = p.getAge();

        ///ASSERT///

        assertEquals(true, personAge > 20);
    }
    
    @Test
    public void personAge_returnZeroIfDateIsNull() {
        ///ARRANGE///

        int personAge = -1;

        Person p = new Person();

        ///ACT///

        personAge = p.getAge();

        ///ASSERT///

        assertEquals(true, personAge == 0);
    }
}
