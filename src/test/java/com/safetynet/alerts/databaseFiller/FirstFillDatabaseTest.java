package com.safetynet.alerts.databaseFiller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.service.PersonService;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FirstFillDatabaseTest {
    
    @Mock
    private PersonService personService;
    @Mock
    private FirestationService firestationService;
    @Mock
    private MedicalRecordService medicalRecordService;
    
    @InjectMocks
    private FirstFillDatabase filler;

    @Test
    public void isDataPresentInDatabaseTest_falseCase() {
        ///ARRANGE///

        boolean isDataPresent;

        when(personService.getPersons()).thenReturn(new ArrayList<>());
        when(firestationService.getFirestations()).thenReturn(new ArrayList<>());
        when(medicalRecordService.getMedicalRecords()).thenReturn(new ArrayList<>());

        ///ACT///

        isDataPresent = filler.isDataPresentInDatabase();

        ///ASSERT///

        assertEquals(false, isDataPresent);
    }

    @Test
    public void isDataPresentInDatabaseTest_trueCase() {
        ///ARRANGE///

        boolean isDataPresent;
        List<Person> persons = new ArrayList<>();

        Person p = new Person();
        persons.add(p);

        when(personService.getPersons()).thenReturn(persons);

        ///ACT///

        isDataPresent = filler.isDataPresentInDatabase();

        ///ASSERT///

        assertEquals(true, isDataPresent);
    }
}