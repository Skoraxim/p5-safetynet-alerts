package com.safetynet.alerts.databaseFiller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;

public class ListToPostGeneratorTest {
    private static ListToPostGenerator generator;

    @BeforeAll
    public static void setUp() {
        generator = new ListToPostGenerator();
    }
    
    @Test
    public void createListsTest() {
        List<Person> personList = null;

        List<Firestation> firestationList = generator.createFirestationList();
        List<MedicalRecord> medicRecordList = generator.createMedicalrecordList();

        try {
            personList = generator.createPersonList();
        } catch (Exception e) {

        }

        assertEquals(true, firestationList != null);
        assertEquals(true, medicRecordList != null);
        assertEquals(true, personList != null);
    }

    @Test
    public void createPersonListTest_withoutOthersBefore_throwException() {
        assertThrows(Exception.class, () -> generator.createPersonList());
    }
}
