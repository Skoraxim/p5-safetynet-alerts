package com.safetynet.alerts.databaseFiller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class JsonFileGetterTest {
    private static JsonFileGetter getter;

    @BeforeAll
    private static void setUp() {
        getter = new JsonFileGetter();
    }

    @Test
    public void getPersonJsonTest() {
        File personFile = getter.getPersonJson();

        assertEquals(true, personFile != null);
    }

    @Test
    public void getFirestationJsonTest() {
        File firestationFile = getter.getFirestationJson();

        assertEquals(true, firestationFile != null);
    }

    @Test
    public void getMedicalrecordJsonTest() {
        File medicRecordFile = getter.getMedicalrecordJson();

        assertEquals(true, medicRecordFile != null);
    }
}
