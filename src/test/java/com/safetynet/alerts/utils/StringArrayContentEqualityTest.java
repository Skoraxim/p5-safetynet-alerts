package com.safetynet.alerts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class StringArrayContentEqualityTest {
    
    @Test
    public void testContentEqualityTest() {
        String[] array1 = {"val1", "val2", "val3"};
        String[] array2 = {"val1", "val2", "val3"};
        String[] array3 = {"val1", "val2", "bonjour"};
        String[] array4 = {"val1", "val2", "val3", "val4"};

        assertEquals(true, StringArrayContentEquality.testContentEquality(array1, array2));
        assertEquals(false, StringArrayContentEquality.testContentEquality(array1, array3));
        assertEquals(false, StringArrayContentEquality.testContentEquality(array1, array4));
    }
}
