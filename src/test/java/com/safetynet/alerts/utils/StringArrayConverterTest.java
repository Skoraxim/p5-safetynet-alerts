package com.safetynet.alerts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class StringArrayConverterTest {

    private static StringArrayConverter converter;
    @BeforeAll
    private static void setUp() {
        converter = new StringArrayConverter();
    }

    @Test
    public void convertToStringArrayTest() {
        ///ARRANGE///
        ObjectMapper mapper = new ObjectMapper();

        ArrayNode rootArrayMedicalrecord = null;

        try {
            rootArrayMedicalrecord = (ArrayNode)mapper.readTree(new File("src/test/resources/medicalrecord_data_test.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayNode medics = (ArrayNode) rootArrayMedicalrecord.get(0).path("medications");

        String[] expectedArray = {"aznol:350mg", "hydrapermazol:100mg"};

        ///ACT///

        String[] resultArray = converter.convertToStringArray(medics);

        ///ASSERT///
        
        assertEquals(true, StringArrayContentEquality.testContentEquality(expectedArray, resultArray));
    }

    @Test
    public void convertToDatabaseColumnTest() {
        String[] array = {"value1", "value2", "value3"};

        String stringVerOfArray = converter.convertToDatabaseColumn(array);

        assertEquals("value1,value2,value3", stringVerOfArray);
    }

    @Test
    public void convertToEntityAttributeTest() {
        String stringVerOfArray = "value1,value2,value3";
        String[] expected = {"value1", "value2", "value3"};

        String[] array = converter.convertToEntityAttribute(stringVerOfArray);


        assertEquals(true, StringArrayContentEquality.testContentEquality(array, expected));
    }

    @Test
    public void convertToDatabaseColumnTest_returnEmptyDatabaseStringIfArrayIsEmpty() {
        String[] emptyArray = {""};
        String[] nullArray = null;

        assertEquals("", converter.convertToDatabaseColumn(emptyArray));
        assertEquals("", converter.convertToDatabaseColumn(nullArray));
    }

    @Test
    public void convertToEntityAttributeTest_returnEmptyStringArrayIfColumnIsEmpty() {
        String emptyString = "";
        String nullString = null;
        String[] expected = {""};

        String[] arrayFromEmpty = converter.convertToEntityAttribute(emptyString);
        String[] arrayFromNull = converter.convertToEntityAttribute(nullString);

        assertEquals(true, StringArrayContentEquality.testContentEquality(arrayFromEmpty, expected));
        assertEquals(true, StringArrayContentEquality.testContentEquality(arrayFromNull, expected));
    }
}
