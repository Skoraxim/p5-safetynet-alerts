package com.safetynet.alerts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;

import org.junit.jupiter.api.Test;

public class StringToDateConverterTest {
    @Test
    public void convertToDateTest() {
        ///ARRANGE///

        StringToDateConverter converter = new StringToDateConverter();
        String dateToConvert = "01/01/2000"; //MM/DD/YYYY

        ///ACT///

        Date date = converter.convertToDate(dateToConvert);

        ///ASSERT///

        assertEquals("2000-01-01", date.toString());
    }
    
    @Test
    public void convertToDateTest_ifDateFormatIsWrong_returnNull() {
        ///ARRANGE///

        StringToDateConverter converter = new StringToDateConverter();
        String dateToConvert = "18/30/2000"; //MM/DD/YYYY, there is no 18th month in a year.

        ///ACT///

        Date date = converter.convertToDate(dateToConvert);

        ///ASSERT///

        assertEquals(null, date);
    }
}
