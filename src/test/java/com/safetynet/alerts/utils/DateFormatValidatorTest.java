package com.safetynet.alerts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

public class DateFormatValidatorTest {

    @Test
    public void isValidTest() {
        String validDate = "01/01/2000";
        String incorrectDate = "18/01/2000"; //There is not an 18th month in a year;

        boolean shouldBeValid;
        try {
            shouldBeValid = DateFormatValidator.isValid(validDate);
        } catch (ParseException e) {
            shouldBeValid = false;
        }
        
        boolean shouldBeInvalid;
        try {
            shouldBeInvalid = DateFormatValidator.isValid(incorrectDate);
        } catch (ParseException e) {
            shouldBeInvalid = false;
        }

        assertEquals(true, shouldBeValid);
        assertEquals(false, shouldBeInvalid);
    }
}
