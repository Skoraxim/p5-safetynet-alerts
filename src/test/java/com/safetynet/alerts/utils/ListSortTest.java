package com.safetynet.alerts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;

public class ListSortTest {

    @Test
    public void personSortByAddressTest() {
        List<Person> defList = new ArrayList<>();
        List<Person> sortList = new ArrayList<>();

        Person p1 = new Person();
        Person p2 = new Person();
        Person p3 = new Person();
        Person p4 = new Person();
        Person p5 = new Person();

        p1.setAddress("1509 Culver St");
        p2.setAddress("29 15th St");
        p3.setAddress("29 15th St");
        p4.setAddress("1509 Culver St");
        p5.setAddress("947 E. Rose Dr");

        defList.add(p1);
        defList.add(p2);
        defList.add(p3);
        defList.add(p4);
        defList.add(p5);

        sortList = ListSort.personSortByAddress(defList);

        assertEquals(false, sortList.get(3).toString().equals(defList.get(3).toString()));
    }

    @Test
    public void personSortByAgeTest() {
        List<Person> defaultList = new ArrayList<>();
        List<Person> sortedList = new ArrayList<>();

        Person p1 = new Person();
        Person p2 = new Person();
        Person p3 = new Person();

        p1.setBirthDate("01/07/2000");
        p2.setBirthDate("01/07/1990");
        p3.setBirthDate("01/07/2010");

        defaultList.add(p1);
        defaultList.add(p2);
        defaultList.add(p3);

        sortedList = ListSort.personSortByAge(defaultList);

        assertEquals(true, defaultList.get(0) == p1 && defaultList.get(1) == p2 && defaultList.get(2) == p3);
        assertEquals(true, sortedList.get(0) == p3 && sortedList.get(1) == p1 && sortedList.get(2) == p2);
    }

    @Test
    public void firestationSortByNumberTest() {
        List<Firestation> defaultList = new ArrayList<>();
        List<Firestation> sortedList = new ArrayList<>();

        Firestation f1 = new Firestation();
        Firestation f2 = new Firestation();
        Firestation f3 = new Firestation();

        f1.setId(1);
        f2.setId(6);
        f3.setId(4);

        defaultList.add(f1);
        defaultList.add(f2);
        defaultList.add(f3);

        sortedList = ListSort.firestationSortByNumber(defaultList);

        assertEquals(true, defaultList.get(0) == f1 && defaultList.get(1) == f2 && defaultList.get(2) == f3);
        assertEquals(true, sortedList.get(0) == f1 && sortedList.get(1) == f3 && sortedList.get(2) == f2);
    } 
}
