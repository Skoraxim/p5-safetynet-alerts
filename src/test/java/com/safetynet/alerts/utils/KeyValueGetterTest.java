package com.safetynet.alerts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class KeyValueGetterTest {
    private static Map<Integer, String> map;

    @BeforeAll
    private static void setUp() {
        map = new HashMap<>();
        map.put(1, "pomme");
        map.put(2, "poire");
        map.put(3, "cerise");
    }

    @Test
    public void getKeyByValueTest() {
        assertEquals(3, KeyValueGetter.getKeyByValue(map, "cerise"));
    }
    @Test

    public void getKeyByValueTest_returnNullIfDoesNotExist() {
        assertEquals(null, KeyValueGetter.getKeyByValue(map, "ananas"));
    }
}
