package com.safetynet.alerts.IT;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.repository.PersonsRepository;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.service.PersonService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MedicalReordControllerIT {
    private static final Logger log = LoggerFactory.getLogger(MedicalReordControllerIT.class);
    
    @Autowired
    private PersonService personService;
    @Autowired
    private MedicalRecordService medicalRecordService;

    @Autowired
    private PersonsRepository personsRepositoryForTeardown;

    private static HttpResponse response;
    private static final String uri = "http://localhost:8080/medicalRecord";
    private static final String strEntityMedicalRecord = "{ \"medications\" : [\"med1:350mg\", \"med2:100mg\"], \"allergies\" : [\"nil\"]}";
    private static final String strEntityMedicalRecordWrongData = "{ \"maidication\" : [\"az:350mg\", \"hydra:100mg\"], \"allergies\" : [\"nil\"]}";
    private static final String strEntityMedicalRecordEmpty = "{ \"medications\" : [], \"allergies\" : []}";





    @BeforeEach
    public void setUp() {
        response = null;
    }

    @Test
    public void createTest() {
        ///ARRANGE///

        HttpPost httpPost = new HttpPost(uri);
        setHeaders(httpPost);

        try {
            httpPost.setEntity(new StringEntity(strEntityMedicalRecord));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createTest_WorkWithEmptyArrays() {
        ///ARRANGE///

        HttpPost request = new HttpPost(uri);
        setHeaders(request);

        try {
            request.setEntity(new StringEntity(strEntityMedicalRecordEmpty));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(request);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createTest_ExceptionIfInvalidData() {
        ///ARRANGE///

        HttpPost request = new HttpPost(uri);
        setHeaders(request);

        try {
            request.setEntity(new StringEntity(strEntityMedicalRecordWrongData));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(request);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatusLine().getStatusCode());
    }

    @Test
    public void mapPersonToMedicalRecordTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        MedicalRecord m = createDummyMedicalRecord();

        int pId = personService.savePerson(p).getId();
        int mId = medicalRecordService.saveMedicalRecord(m).getId();

        HttpPost httpPost = new HttpPost(uri + "/" + pId + "/" + mId);
        setHeaders(httpPost);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void mapPersonToMedicalRecordTest_ExceptionIfRecordDoesNotExist() {
        ///ARRANGE///

        Person p = createDummyPerson();

        int pId = personService.savePerson(p).getId();

        HttpPost httpPost = new HttpPost(uri + "/" + pId + "/2077");
        setHeaders(httpPost);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void mapPersonToMedicalRecordTest_ExceptionIfPersonDoesNotExist() {
        ///ARRANGE///

        MedicalRecord m = createDummyMedicalRecord();

        int mId = medicalRecordService.saveMedicalRecord(m).getId();

        HttpPost httpPost = new HttpPost(uri + "/2077/" + mId);
        setHeaders(httpPost);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAllTest() {
        ///ARRANGE///

        HttpGet httpGet = new HttpGet(uri);
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getByIdTest() {
        ///ARRANGE///

        MedicalRecord m = createDummyMedicalRecord();
        int mId = medicalRecordService.saveMedicalRecord(m).getId();

        HttpGet httpGet = new HttpGet(uri + "/" + mId);
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getByIdTest_ExceptionIfRecordDoesNotExist() {
        ///ARRANGE///

        HttpGet httpGet = new HttpGet(uri + "/2077");
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void updateTest() {
        MedicalRecord m = createDummyMedicalRecord();
        int mId = medicalRecordService.saveMedicalRecord(m).getId();

        HttpPut httpPut = new HttpPut(uri + "/" + mId);
        setHeaders(httpPut);

        try {
            httpPut.setEntity(new StringEntity(strEntityMedicalRecord));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        try {
            response = HttpClientBuilder.create().build().execute(httpPut);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }
    
    @Test
    public void deleteTest() {
        ///ARRANGE///

        MedicalRecord m = createDummyMedicalRecord();
        Person p = createDummyPerson();

        int mId = medicalRecordService.saveMedicalRecord(m).getId();
        int pId = personService.savePerson(p).getId();
        
        medicalRecordService.medicalRecordToPersonMapping(pId, mId);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("firstName" , p.getFirstName()));
        params.add(new BasicNameValuePair("lastName", p.getLastName()));

        HttpDelete httpDelete = new HttpDelete(uri);
        setHeaders(httpDelete);
        
        try {
            URI uriWithParams = new URIBuilder(httpDelete.getURI()).addParameters(params).build();
            ((HttpRequestBase) httpDelete).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteTest_AlsoSetPersonRecordAsNull() {
        ///ARRANGE///

        MedicalRecord m = createDummyMedicalRecord();
        Person p = createDummyPerson();

        int mId = medicalRecordService.saveMedicalRecord(m).getId();
        int pId = personService.savePerson(p).getId();
        
        medicalRecordService.medicalRecordToPersonMapping(pId, mId);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("firstName" , p.getFirstName()));
        params.add(new BasicNameValuePair("lastName", p.getLastName()));

        HttpDelete httpDelete = new HttpDelete(uri);
        setHeaders(httpDelete);
        
        try {
            URI uriWithParams = new URIBuilder(httpDelete.getURI()).addParameters(params).build();
            ((HttpRequestBase) httpDelete).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        MedicalRecord recordResult = personService.getPerson(pId).get().getMedicalRecord();
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertEquals(true, recordResult == null);
    }

    @AfterAll
    public void teardown() {
        List<Person> personToDelete = (List<Person>) personsRepositoryForTeardown.findByAddress(createDummyPerson().getAddress());
        Map<String, String> fullName = null;
        
        for (Person p : personToDelete) {
            fullName = new HashMap<>();

            fullName.put("firstName", p.getFirstName());
            fullName.put("lastName", p.getLastName());

            personService.deletePerson(fullName);
        }
    }
    




    private Person createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setFirstName(pickRandom(names) + pickRandom(names)+ pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");
        p.setId(new Random().nextInt(2000));

        return p;
    }

    private MedicalRecord createDummyMedicalRecord() {
        MedicalRecord m = new MedicalRecord();
        String[] allergies = {"allergie1", "allergie2"};
        String[] medications = {"medication1:100mg", "medication2:200kg", "medication3:1g"};

        m.setAllergies(allergies);
        m.setMedications(medications);
        m.setId(new Random().nextInt(2000));

        return m;
    }

    private String pickRandom(List<String> list) {
        Random random = new Random();

        return list.get(random.nextInt(list.size()));
    }

    private HttpRequestBase setHeaders(HttpRequestBase request) {
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        return request;
    }
}
