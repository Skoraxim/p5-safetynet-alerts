package com.safetynet.alerts.IT;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.PersonService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class FirestationControllerIT {
    private static final Logger log = LoggerFactory.getLogger(FirestationControllerIT.class);
    
    @Autowired
    private PersonService personService;
    @Autowired
    private FirestationService firestationService;

    private static HttpResponse response;
    private static final String uri = "http://localhost:8080/firestation";





    @BeforeEach
    public void setUp() {
        response = null;
    }

    @Test
    public void createFirestationTest() {
        ///ARRANGE///

        HttpPost httpPost = new HttpPost(uri);

        try {
            httpPost.setEntity(new StringEntity("{}"));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        setHeaders(httpPost);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createFirestationToPersonMappingTest() {
        ///ARRANGE///

        Person person = createDummyPerson();
        Firestation firestation = new Firestation();

        int pId = personService.savePerson(person).getId();
        int fId = firestationService.saveFirestation(firestation).getId();

        HttpPost httpPost = new HttpPost(uri + "/" + fId + "/" + pId);
        setHeaders(httpPost);
        
        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createMappingTest_ExcpetionIfFirestationDoesNotExist() {
        ///ARRANGE///

        Person person = createDummyPerson();

        int pId = personService.savePerson(person).getId();

        HttpPost request = new HttpPost(uri + "/80/" + pId);
        setHeaders(request);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(request);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createMappingTest_ExcpetionIfPersonDoesNotExist() {
        ///ARRANGE///

        Firestation firestation = new Firestation();

        int fId = firestationService.saveFirestation(firestation).getId();

        HttpPost request = new HttpPost(uri + "/" + fId + "/223");
        setHeaders(request);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(request);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///
        
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createMappingTest_ExceptionIfPersonIsAlreadyMappedToAStation() {
        ///ARRANGE///
        
        Person person = createDummyPerson();
        Firestation firestation = new Firestation();
        Firestation firestation2 = new Firestation();

        int pId = personService.savePerson(person).getId();
        int fId = firestationService.saveFirestation(firestation).getId();
        int fId2 = firestationService.saveFirestation(firestation2).getId();

        firestationService.createFirestationToPersonMapping(fId, pId);

        HttpPost httpPost = new HttpPost(uri + "/" + fId2 + "/" + pId);
        setHeaders(httpPost);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_METHOD_NOT_ALLOWED, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAllTest() {
        ///ARRANGE///

        HttpGet httpGet = new HttpGet(uri);
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getByIdTest() {
        ///ARRANGE///

        Firestation f = new Firestation();
        int fId = firestationService.saveFirestation(f).getId();

        HttpGet httpGet = new HttpGet(uri + "/" + fId);

        setHeaders(httpGet);
        
        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getByIdTest_ExceptionIfNotFound() {
        ///ARRANGE///

        HttpGet httpGet = new HttpGet(uri + "/2390");
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void updateFirestationToPersonMappingTest() {
        ///ARRANGE///

        Person person = createDummyPerson();
        Firestation firestation = new Firestation();

        int pId = personService.savePerson(person).getId();
        int fId = firestationService.saveFirestation(firestation).getId();

        HttpPut httpPut = new HttpPut(uri + "/" + fId + "/" + pId);
        setHeaders(httpPut);
        
        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPut);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteTest() {
        ///ARRANGE///

        Firestation firestation = new Firestation();
        int fId = firestationService.saveFirestation(firestation).getId();

        HttpDelete httpDelete = new HttpDelete(uri + "/" + fId);
        setHeaders(httpDelete);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteTest_ExceptionIfDoesNotExist() {
        ///ARRANGE///

        HttpDelete request = new HttpDelete(uri+"/2004");
        setHeaders(request);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(request);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteTest_ExceptionIfIsMappedToAtLeastOnPerson() {
        ///ARRANGE///

        Person person = createDummyPerson();
        int pId = personService.savePerson(person).getId();

        Firestation firestation = new Firestation();
        int fId = firestationService.saveFirestation(firestation).getId();

        firestationService.createFirestationToPersonMapping(fId, pId);

        HttpDelete httpDelete = new HttpDelete(uri + "/" + fId);
        setHeaders(httpDelete);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_CONFLICT, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteFirestationToPersonMappingTest() {
        ///ARRANGE///

        Person person = createDummyPerson();
        Firestation firestation = new Firestation();

        int pId = personService.savePerson(person).getId();
        int fId = firestationService.saveFirestation(firestation).getId();

        HttpDelete httpDelete = new HttpDelete(uri + "/" + fId + "/" + pId);
        setHeaders(httpDelete);
        
        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }


    /////ADDITIONAL ENDPOINT/////

    @Test
    public void getPersonDetailsByStationIdTest() {
        ///ARRANGE///

        Person person1 = createDummyPerson();
        Person person2 = createDummyPerson();
        Firestation firestation = new Firestation();

        String fId = ((Integer) firestationService.saveFirestation(firestation).getId()).toString();
        personService.savePerson(person1);
        personService.savePerson(person2);

        HttpGet httpGet = new HttpGet(uri);
        setHeaders(httpGet);

        try {
            URI uriWithParams = new URIBuilder(httpGet.getURI()).addParameter("stationNumber", fId).build();
            ((HttpRequestBase) httpGet).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getPersonsDetailsByStationIdTest_ExceptionIfStationDoesNotExist() {
        ///ARRANGE///

        Person person1 = createDummyPerson();
        Person person2 = createDummyPerson();

        personService.savePerson(person1);
        personService.savePerson(person2);

        HttpGet httpGet = new HttpGet(uri);
        setHeaders(httpGet);

        try {
            URI uriWithParams = new URIBuilder(httpGet.getURI()).addParameter("stationNumber", "1794").build();
            ((HttpRequestBase) httpGet).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    
    


    private Person createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setFirstName(pickRandom(names) + pickRandom(names) + pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");

        return p;
    }

    private String pickRandom(List<String> list) {
        Random random = new Random();

        return list.get(random.nextInt(list.size()));
    }

    private HttpRequestBase setHeaders(HttpRequestBase request) {
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        return request;
    }
}
