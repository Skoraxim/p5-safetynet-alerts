package com.safetynet.alerts.IT;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.PersonService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class AdditionalEndpointsControllerIT {
    private static final Logger log = LoggerFactory.getLogger(AdditionalEndpointsControllerIT.class);
    
    @Autowired
    private PersonService personService;
    @Autowired
    private FirestationService firestationService;

    private static HttpResponse response;
    private static final String uri = "http://localhost:8080";




    
    @BeforeEach
    public void setUp() {
        response = null;
    }

    @Test
    public void getChildsAndOthersMembersByAddressTest() {
        ///ARRANGE///

        Person p = createDummyPerson();

        String address = personService.savePerson(p).getAddress();

        HttpGet request = new HttpGet(uri + "/childAlert");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("address", address).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getChildsAndOthersMembersByAddressTest_ExceptionIfAddressDoesNotExist() {
        ///ARRANGE///
        Person p = createDummyPerson();

        personService.savePerson(p);

        HttpGet request = new HttpGet(uri + "/childAlert");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("address", "96 rue de là bas").build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());

    }

    @Test
    public void getChildsAndOthersMembersByAddressTest_ReturnsNullIfNoChildsExists() {
        ///ARRANGE///
        Person p = createDummyPerson();

        String address = personService.savePerson(p).getAddress();

        HttpGet request = new HttpGet(uri + "/childAlert");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("address", address).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertEquals(true, response.getEntity().getContentLength() == 0);
    }

    @Test
    public void getPhoneNumbersForAFirestationTest() {
        ///ARRANGE///

        Firestation f = new Firestation();
        Person p = createDummyPerson();

        Integer fId = firestationService.saveFirestation(f).getId();
        int pId = personService.savePerson(p).getId();

        firestationService.updateFirestationToPersonMapping(fId, pId);

        HttpGet request = new HttpGet(uri + "/phoneAlert");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("firestation", fId.toString()).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getPhoneNumbersForAFirestationTest_ReturnsNullIfNothingExists() {
        ///ARRANGE///

        Firestation f = new Firestation();

        Integer fId = firestationService.saveFirestation(f).getId();

        HttpGet request = new HttpGet(uri + "/phoneAlert");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("firestation", fId.toString()).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertEquals(true, response.getEntity().getContentLength() == 0);
    }

    @Test
    public void getFirestationWithPersonsDetailsTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        Person p2 = createDummyPerson();
        p2.setFirstName("Moi");

        String pAddress = personService.savePerson(p).getAddress();
        personService.savePerson(p2);

        HttpGet request = new HttpGet(uri + "/fire");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("address", pAddress).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getPersonsDetailsForSpecifiedStationsTest() {
        ///ARRANGE///
        Person p = createDummyPerson();
        Person p2 = createDummyPerson();
        p2.setFirstName("Pasmoi");

        int pId = personService.savePerson(p).getId();
        int p2Id = personService.savePerson(p2).getId();

        Firestation f = new Firestation();
        int fId = firestationService.saveFirestation(f).getId();

        firestationService.updateFirestationToPersonMapping(fId, pId);
        firestationService.updateFirestationToPersonMapping(fId, p2Id);

        HttpGet request = new HttpGet(uri  + "/flood/stations");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("stations", "1").build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getPersonDetailsLimitedTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        Person savedP = personService.savePerson(p);

        HttpGet request = new HttpGet(uri + "/personInfo");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("firstName", savedP.getFirstName()).addParameter("lastName", savedP.getLastName()).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAllEmailsForACityTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        Person p2 = createDummyPerson();
        p2.setEmail("dummy@mail.miam");

        String pCity = personService.savePerson(p).getCity();
        personService.savePerson(p2);

        HttpGet request = new HttpGet(uri + "/communityEmail");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("city", pCity).build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///
        
        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAllEmailsForACityTest_ExceptionIfCityDoesNotExistsOrNoPerson() {
        ///ARRANGE///

        Person p = createDummyPerson();
        Person p2 = createDummyPerson();
        p2.setEmail("dummy@mail.miam");

        personService.savePerson(p);
        personService.savePerson(p2);

        HttpGet request = new HttpGet(uri + "/communityEmail");
        setHeaders(request);

        try {
            URI uriWithParams = new URIBuilder(request.getURI()).addParameter("city", "sittie").build();
            ((HttpRequestBase) request).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        response = createBuildExec(request);

        ///ASSERT///
        
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }





    private Person createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setFirstName(pickRandom(names) + pickRandom(names)+ pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");

        return p;
    }

    private String pickRandom(List<String> list) {
        Random random = new Random();

        return list.get(random.nextInt(list.size()));
    }

    private HttpRequestBase setHeaders(HttpRequestBase request) {
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        return request;
    }

    private HttpResponse createBuildExec(HttpRequestBase request) {
        try {
            return HttpClientBuilder.create().build().execute(request);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        return null;
    }
}
