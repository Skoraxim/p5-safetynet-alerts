package com.safetynet.alerts.IT;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.safetynet.alerts.databaseFiller.FirstFillDatabase;
import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.service.PersonService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class FirstFillDatabaseIT {
    public static final Logger log = LoggerFactory.getLogger(FirstFillDatabase.class);

    @Autowired
    private FirstFillDatabase filler;

    @Autowired
    private FirestationService firestationService;
    @Autowired
    private MedicalRecordService medicalRecordService;
    @Autowired
    private PersonService personService;

    @Test
    public void dataGenerationAndSavingWork() {
        ///ACT///

        filler.generateData();

        List<Person> persons = new ArrayList<>();
        persons = (List<Person>) personService.getPersons();

        List<Firestation> firestations = new ArrayList<>();
        firestations = (List<Firestation>) firestationService.getFirestations();

        List<MedicalRecord> records = new ArrayList<>();
        records = (List<MedicalRecord>) medicalRecordService.getMedicalRecords();

        ///ASSERT///

        assertEquals(true, persons.size() != 0);
        assertEquals(true, firestations.size() != 0);
        assertEquals(true, records.size() != 0);
    }
}
