package com.safetynet.alerts.IT;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.service.PersonService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class PersonControllerIT {
    private static final Logger log = LoggerFactory.getLogger(PersonControllerIT.class);
    
    @Autowired
    private PersonService personService;
    @Autowired
    private MedicalRecordService medicalRecordService;
    @Autowired
    private FirestationService firestationService;

    private static HttpResponse response;
    private static final String uri = "http://localhost:8080/person";
    private static final String strEntityPerson = "{\"firstName\" : \"blabla\",\"lastName\" : \"slurp\",\"birthDate\" : \"03/06/1984\",\"address\" : \"1 Rue de Paris\",\"city\" : \"Culver\",\"zipCode\" : \"97451\",\"phoneNumber\" : \"841-874-6512\",\"email\" : \"blablaslurp@email.com\"}";
    private static final String strEntityPerson2 = "{\"firstName\" : \"Noah\",\"lastName\" : \"Comas\",\"birthDate\" : \"03/06/1984\",\"address\" : \"1 Rue de Paris\",\"city\" : \"Culver\",\"zipCode\" : \"97451\",\"phoneNumber\" : \"841-874-6512\",\"email\" : \"noahcomas@email.com\"}";
    private static final String strEntityPersonWrongData = "{\"feurstName\" : \"John\",\"lastName\" : \"Boyd\",\"birthDate\" : \"03/06/1984\",\"address\" : \"1 Rue de Paris\",\"city\" : \"Culver\",\"zipCode\" : \"97451\",\"phoneNumber\" : \"841-874-6512\",\"email\" : \"jaboyd@email.com\"}";





    @BeforeEach
    public void setUp() {
        response = null;
    }
    
    @Test
    public void createTest() {
        ///ARRANGE///

        HttpPost httpPost = new HttpPost(uri);
        setHeaders(httpPost);

        try {
            httpPost.setEntity(new StringEntity(strEntityPerson));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void createTest_ExceptionIfInvalidData() {
        ///ARRANGE///

        HttpPost httpPost = new HttpPost(uri);
        setHeaders(httpPost);

        try {
            httpPost.setEntity(new StringEntity(strEntityPersonWrongData));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPost);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getAllTest() {
        ///ARRANGE///

        HttpGet httpGet = new HttpGet(uri);
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getByIdTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        int pId = personService.savePerson(p).getId();

        HttpGet httpGet = new HttpGet(uri + "/" + pId);
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getByIdTest_ExceptionIfPersonDoesNotExist() {
        ///ARRANGE///
        HttpGet httpGet = new HttpGet(uri + "/1234");
        setHeaders(httpGet);

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpGet);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void updateTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        int pId = personService.savePerson(p).getId();

        HttpPut httpPut = new HttpPut(uri + "/" + pId);
        setHeaders(httpPut);

        try {
            httpPut.setEntity(new StringEntity(strEntityPerson2));
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpPut);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteTest() {
        ///ARRANGE///

        Person p = createDummyPerson();
        personService.savePerson(p);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("firstName" , p.getFirstName()));
        params.add(new BasicNameValuePair("lastName", p.getLastName()));

        HttpDelete httpDelete = new HttpDelete(uri);
        setHeaders(httpDelete);
        
        try {
            URI uriWithParams = new URIBuilder(httpDelete.getURI()).addParameters(params).build();
            ((HttpRequestBase) httpDelete).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }

    @Test
    public void deleteTest_AlsoDeleteFromFirestationAndMedicalRecord() {
        ///ARRANGE///

        MedicalRecord m = createDummyMedicalRecord();
        MedicalRecord savedM = medicalRecordService.saveMedicalRecord(m);

        Firestation f = new Firestation();
        int fId = firestationService.saveFirestation(f).getId();

        Person p = createDummyPerson();
        int pId = personService.savePerson(p).getId();

        firestationService.createFirestationToPersonMapping(fId, pId);
        medicalRecordService.medicalRecordToPersonMapping(pId, savedM.getId());

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("firstName" , p.getFirstName()));
        params.add(new BasicNameValuePair("lastName", p.getLastName()));

        HttpDelete httpDelete = new HttpDelete(uri);
        setHeaders(httpDelete);
        
        try {
            URI uriWithParams = new URIBuilder(httpDelete.getURI()).addParameters(params).build();
            ((HttpRequestBase) httpDelete).setURI(uriWithParams);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e.getCause());
        }

        ///ACT///

        try {
            response = HttpClientBuilder.create().build().execute(httpDelete);
        } catch (ClientProtocolException e) {
            log.error(e.getMessage(), e.getCause());
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }
        
        ///ASSERT///

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
        assertEquals(false, firestationService.getFirestation(fId).get().getPersons().contains(p));
        assertEquals(false, ((List<MedicalRecord>) medicalRecordService.getMedicalRecords()).contains(savedM));
    }





    private Person createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setFirstName(pickRandom(names) + pickRandom(names) + pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");

        return p;
    }

    private MedicalRecord createDummyMedicalRecord() {
        String[] medications = {"medication1:100mg", "medication2:200mg"};
        String[] allergies = {"allergie1", "allergie2"};

        MedicalRecord m = new MedicalRecord();
        m.setAllergies(allergies);
        m.setMedications(medications);
        m.setId(new Random().nextInt(2000));

        return m;
    }

    private String pickRandom(List<String> list) {
        Random random = new Random();

        return list.get(random.nextInt(list.size()));
    }

    private HttpRequestBase setHeaders(HttpRequestBase request) {
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        return request;
    }
}
