package com.safetynet.alerts.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import com.safetynet.alerts.model.Firestation;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.model.CustomEndpointsObjects.FirestationWithPersonDetailsForGetMappingStationNumber;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonsDetailsForSpecifiedStations;
import com.safetynet.alerts.repository.FirestationsRepository;
import com.safetynet.alerts.service.FirestationService;
import com.safetynet.alerts.service.PersonService;

@ExtendWith(MockitoExtension.class)
public class FirestationServiceTest {
    private Random random = new Random();

    @Mock
    public FirestationsRepository firestationsRepository;

    @InjectMocks
    @Resource
    private FirestationService firestationService;
    @Mock
    private PersonService personService;





    @Test
    public void getFirestationsTest() {
        ///ARRANGE///
        Firestation f = createDummyFirestation().get();

        List<Firestation> fs = new ArrayList<>();
        List<Firestation> firestations = new ArrayList<>();
        
        fs.add(f);
        firestations.add(f);

        when(firestationsRepository.findAll()).thenReturn(fs);

        ///ACT & ASSERT///

        assertEquals(true, firestations.equals(firestationService.getFirestations()));
    }

    @Test
    public void getFirestationTest() {
        ///ARRANGE///

        Optional<Firestation> f = createDummyFirestation();

        Firestation firestation = new Firestation();

        when(firestationsRepository.findById(anyInt())).thenReturn(f);

        ///ACT///

        firestation = firestationService.getFirestation(f.get().getId()).get();


        ///ASSERT///

        assertEquals(true, firestation.equals(f.get()));

    }

    @Test
    public void saveFirestationTest() {
        ///ARRANGE///

        Firestation f = createDummyFirestation().get();

        when(firestationsRepository.save(any(Firestation.class))).thenReturn(f);

        ///ACT///

        Firestation result = firestationService.saveFirestation(f);

        ///ASSERT///

        assertEquals(true, result.equals(f));
    }

    @Test
    public void createFirestationToPersonMappingTest() {
        ///ARRANGE///

        Person person = createDummyPerson();
        Firestation firestation = new Firestation();

        when(personService.getPerson(anyInt())).thenReturn(Optional.ofNullable(person));
        when(firestationsRepository.findById(anyInt())).thenReturn(Optional.ofNullable(firestation));

        when(personService.savePerson(any(Person.class))).thenReturn(person);
        when(firestationService.saveFirestation(any(Firestation.class))).thenReturn(firestation);

        person = personService.savePerson(person);
        firestation = firestationService.saveFirestation(firestation);
        
        ///ACT///

        firestationService.createFirestationToPersonMapping(firestation.getId(), person.getId());

        ///ASSERT///

        assertEquals(true, firestation.getPersons().get(0).equals(person));
    }

    @Test
    public void updateFirestationTest() {
        ///ARRANGE///

        List<Person> updatedFsPersons = null;

        Optional<Firestation> f = createDummyFirestation();
        Firestation baseFirestation = f.get();

        Firestation updatedFirestation = createDummyFirestation().get();
        updatedFsPersons = updatedFirestation.getPersons();
        updatedFsPersons.add(createDummyPerson());

        updatedFirestation.setPersons(updatedFsPersons);
        updatedFirestation.setId(baseFirestation.getId());

        Firestation result = new Firestation();

        when(firestationsRepository.save(any(Firestation.class))).thenReturn(baseFirestation, updatedFirestation);
        when(firestationsRepository.findById(anyInt())).thenReturn(f);

        ///ACT///

        firestationService.saveFirestation(baseFirestation);

        result = firestationService.updateFirestation(baseFirestation.getId(), updatedFirestation);

        ///ASSERT///

        assertEquals(true, updatedFirestation.equals(baseFirestation));
        assertEquals(true, updatedFirestation.equals(result));

    }

    @Test
    public void deleteFirestationTest() {
        ///ARRANGE///

        Optional<Firestation> f = createDummyFirestation();
        Firestation firestation = f.get();

        Optional<Firestation> f2 = createDummyFirestation();
        Firestation firestation2 = f2.get();
        firestation2.setId(new Random().nextInt(2000));
        firestation2.setPersons(null);

        int randomId = 40;

        when(firestationsRepository.findById(firestation.getId())).thenReturn(f);
        when(firestationsRepository.findById(firestation2.getId())).thenReturn(f2);
        when(firestationsRepository.findById(randomId)).thenReturn(Optional.empty());

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> firestationService.deleteFirestation(firestation.getId()));
        assertThrows(ResponseStatusException.class, () -> firestationService.deleteFirestation(randomId));
        assertEquals(true, firestationService.deleteFirestation(firestation2.getId()));

    }

    @Test
    public void updateFirestationToPersonMappingTest() {
        ///ARRANGE///

        Optional<Person> p = Optional.ofNullable(new Person());
        Person person = p.get();

        Optional<Firestation> f = Optional.ofNullable(new Firestation());
        Firestation firestation = f.get();

        when(personService.getPerson(anyInt())).thenReturn(p);
        when(firestationsRepository.findById(anyInt())).thenReturn(f);

        ///ACT///

        firestationService.updateFirestationToPersonMapping(firestation.getId(), person.getId());

        ///ASSERT///

        assertEquals(true, firestation.getPersons().get(0).equals(person));
        assertEquals(true, person.getFirestation().equals(firestation));
    }

    @Test
    public void getPersonsDetailsByFirestationTest() {
        ///ARRANGE///

        Optional<Firestation> f = createDummyFirestation();
        Firestation firestation = f.get();

        List<Person> persons = firestation.getPersons();

        for (int i = 0; i < persons.size(); i++) {
            Person p = createDummyPerson();
            p.setId(i+1);

            persons.set(i, p);
        }

        when(firestationsRepository.findById(anyInt())).thenReturn(f);

        ///ACT///

        FirestationWithPersonDetailsForGetMappingStationNumber details = firestationService.getPersonsDetailsByFirestation(firestation.getId());

        ///ASSERT///

        assertEquals(2, details.getAdultCount());
        assertEquals(0, details.getChildCount());
        assertEquals(firestation.getId(), details.getFirestation().getId());
        assertEquals(2, details.getPersons().size());
    }

    @Test
    public void getPersonsDetailsForSpecifiedStationsTest() {
        ///ARRANGE///

        Person p1 = createDummyPerson();
        Person p2 = createDummyPerson();
        Person p3 = createDummyPerson();
        Person p4 = createDummyPerson();

        p1.setId(random.nextInt(2000));
        p2.setId(random.nextInt(2000));
        p3.setId(random.nextInt(2000));
        p4.setId(random.nextInt(2000));

        List<Person> pF1 = new ArrayList<>();
        List<Person> pF2 = new ArrayList<>();

        pF1.add(p1);
        pF1.add(p2);
        pF1.add(p3);
        pF2.add(p4);

        Optional<Firestation> f1 = Optional.ofNullable(new Firestation());
        Optional<Firestation> f2 = Optional.ofNullable(new Firestation());
        Firestation firestation1 = f1.get();
        Firestation firestation2 = f2.get();

        firestation1.setId(random.nextInt(2000));
        firestation2.setId(random.nextInt(2000));

        firestation1.setPersons(pF1);
        firestation2.setPersons(pF2);

        List<Integer> stationsIds = new ArrayList<>();

        stationsIds.add(1);
        stationsIds.add(2);

        when(firestationsRepository.findById(1)).thenReturn(f1);
        when(firestationsRepository.findById(2)).thenReturn(f2);

        ///ACT///

        List<PersonsDetailsForSpecifiedStations> details = firestationService.getPersonsDetailsForSpecifiedStations(stationsIds);

        ///ASSERT///

        assertEquals(4, details.size());
    }

    @Test
    public void getPersonsDetailsForSpecifiedStationsTest_ExceptionIfDoesNotExist() {
        ///ARRANGE///

        List<Integer> fList = new ArrayList<>();
        fList.add(29380);

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> firestationService.getPersonsDetailsForSpecifiedStations(fList));
    }

    @Test
    public void getPhoneNumbersForAStationTest() {
        ///ARRANGE///

        Optional<Firestation> f = Optional.ofNullable(new Firestation());
        Firestation firestation = f.get();

        Person p1 = createDummyPerson();
        Person p2 = createDummyPerson();

        p1.setPhoneNumber("000-111-2222");
        p2.setPhoneNumber("333-444-5555");

        List<Person> persons = new ArrayList<>();

        persons.add(p1);
        persons.add(p2);

        firestation.setPersons(persons);
        firestation.setId(random.nextInt(2000));

        when(firestationsRepository.findById(firestation.getId())).thenReturn(f);

        ///ACT///

        List<String> numbers = firestationService.getPhoneNumbersForAStation(firestation.getId());

        ///ASSERT///
        assertEquals(2, numbers.size());
        assertEquals("000-111-2222", numbers.get(0));
        assertEquals("333-444-5555", numbers.get(1));
    }




    
    private Optional<Firestation> createDummyFirestation() {
        Person p = createDummyPerson();
        Person p2 = createDummyPerson();

        Firestation f = new Firestation();

        List<Person> persons = new ArrayList<>();

        persons.add(p);
        persons.add(p2);
        
        f.setId(new Random().nextInt(2000));
        f.setPersons(persons);

        return Optional.ofNullable(f);
    }

    private Person createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setFirstName(pickRandom(names) + pickRandom(names) + pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");
        p.setId(random.nextInt(2000));

        return p;
    }

    private String pickRandom(List<String> list) {
        Random random = new Random();

        return list.get(random.nextInt(list.size()));
    }
}
