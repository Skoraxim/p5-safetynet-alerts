package com.safetynet.alerts.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import com.safetynet.alerts.model.MedicalRecord;
import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.repository.MedicalRecordsRepository;
import com.safetynet.alerts.service.MedicalRecordService;
import com.safetynet.alerts.service.PersonService;

@ExtendWith(MockitoExtension.class)
public class MedicalRecordServiceTest {

    @Mock
    public MedicalRecordsRepository medicalRecordsRepository;
    @InjectMocks
    @Resource
    private MedicalRecordService medicalRecordService;
    @Mock
    public PersonService personService;

    private Random random = new Random();




    
    @Test
    public void getMedicalRecordsTest() {
        ///ARRANGE///

        MedicalRecord m = createDummyMedicalRecord().get();

        List<MedicalRecord> recordList = new ArrayList<>();
        recordList.add(m);

        when(medicalRecordsRepository.findAll()).thenReturn(recordList);

        ///ACT///

        MedicalRecord recordResult = ((List<MedicalRecord>) medicalRecordService.getMedicalRecords()).get(0);

        ///ASSERT///

        assertEquals(true, recordResult.equals(m));
    }

    @Test
    public void getMedicalRecordTest() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();
        record.setId(random.nextInt(2000));

        when(medicalRecordsRepository.findById(anyInt())).thenReturn(m);

        ///ACT///

        MedicalRecord recordResult = medicalRecordService.getMedicalRecord(1).get();

        ///ASSERT///
        assertEquals(true, record.equals(recordResult));
    }

    @Test
    public void getMedicalRecordByPersonIdTest() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();
        record.setId(random.nextInt(2000));

        Person p = createDummyPerson();
        p.setMedicalRecord(record);

        when(personService.getPerson(p.getId())).thenReturn(Optional.ofNullable(p));
        when(personService.getPerson(123)).thenReturn(Optional.ofNullable(new Person()));

        ///ACT///

        MedicalRecord recordResult = medicalRecordService.getMedicalRecordByPersonId(p.getId()).get();

        ///ASSERT///

        assertEquals(true, recordResult.equals(record));
        assertThrows(ResponseStatusException.class, () -> medicalRecordService.getMedicalRecordByPersonId(123).get());

    }

    @Test
    public void deleteMedicalRecordTest() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();

        Person person = createDummyPerson();

        record.setPerson(person);
        person.setMedicalRecord(record);

        List<MedicalRecord> recordList = new ArrayList<>();

        recordList.add(record);

        Map<String, String> firstNameAndLastName = new HashMap<String, String>();
        firstNameAndLastName.put("firstName", person.getFirstName());
        firstNameAndLastName.put("lastName", person.getLastName());

        when(personService.getPersonByLastNameAndFirstName(any())).thenReturn(Optional.ofNullable(person));
        when(medicalRecordsRepository.findAll()).thenReturn(recordList);

        ///ACT///

        recordList.remove(0);
        medicalRecordService.deleteMedicalRecord(firstNameAndLastName);

        ///ASSERT///

        assertEquals(true, recordList.size() == 0);
        verify(medicalRecordsRepository, times(1)).deleteById(anyInt());
    }

    @Test
    public void deleteMedicalRecordTest_Exceptions() {
        ///ARRANGE///

        Map<String, String> mapWithNotEnoughValues = new HashMap<>();
        mapWithNotEnoughValues.put("firstName", "prénom");

        Map<String, String> firstNameAndLastName = new HashMap<String, String>();
        firstNameAndLastName.put("firstName", "Jean");
        firstNameAndLastName.put("lastName", "Jacques");

        when(personService.getPersonByLastNameAndFirstName(any())).thenReturn(Optional.ofNullable(new Person()));

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> medicalRecordService.deleteMedicalRecord(mapWithNotEnoughValues));
        assertThrows(ResponseStatusException.class, () -> medicalRecordService.deleteMedicalRecord(firstNameAndLastName));
        
    }

    @Test
    public void saveMedicalRecordTest() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();

        when(medicalRecordsRepository.save(any(MedicalRecord.class))).thenReturn(record);

        ///ACT///

        MedicalRecord recordResult = medicalRecordService.saveMedicalRecord(record);

        //ASSERT///

        assertEquals(true, recordResult.equals(record));
    }

    @Test
    public void updateTest() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();

        Optional<MedicalRecord> mUpdated = createDummyMedicalRecord();
        MedicalRecord recordUpdated = mUpdated.get();
        recordUpdated.setId(record.getId());
        recordUpdated.setAllergies(new String[] {""});
        
        when(medicalRecordsRepository.findById(anyInt())).thenReturn(m);
        when(medicalRecordsRepository.save(any(MedicalRecord.class))).thenReturn(recordUpdated);

        ///ACT///

        MedicalRecord recordResult = medicalRecordService.update(record.getId(), recordUpdated);

        ///ASSERT///

        assertEquals(true, recordResult.equals(recordUpdated));
    }

    @Test
    public void medicalRecordToPersonMappingTest() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();

        Optional<Person> p = Optional.ofNullable(createDummyPerson());
        Person person = p.get();

        when(medicalRecordsRepository.findById(anyInt())).thenReturn(m);
        when(personService.getPerson(anyInt())).thenReturn(p);
        when(medicalRecordsRepository.save(any(MedicalRecord.class))).thenReturn(record);

        ///ACT///

        medicalRecordService.medicalRecordToPersonMapping(person.getId(), record.getId());

        ///ASSERT///

        assertEquals(true, person.getMedicalRecord().equals(record));
        assertEquals(true, record.getPerson().equals(person));
    }

    @Test
    public void medicalRecordToPersonMappingTest_ExceptionIfPersonAlreadyHaveAnOtherMedicalRecord() {
        ///ARRANGE///

        Optional<MedicalRecord> m = createDummyMedicalRecord();
        MedicalRecord record = m.get();

        Optional<MedicalRecord> m2 = createDummyMedicalRecord();
        MedicalRecord record2 = m2.get();
        record2.setId(123);

        Optional<Person> p = Optional.ofNullable(createDummyPerson());
        Person person = p.get();
        person.setMedicalRecord(record);

        when(medicalRecordsRepository.findById(anyInt())).thenReturn(m);
        when(personService.getPerson(anyInt())).thenReturn(p);

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> medicalRecordService.medicalRecordToPersonMapping(person.getId(), record2.getId()));
    }




    private Optional<MedicalRecord> createDummyMedicalRecord() {
        MedicalRecord m = new MedicalRecord();

        String[] medications = {"medication1:100mg", "medication2:200kg", "medication3:6g"};
        String[] allergies = {"allergie1", "alergie2"};

        m.setMedications(medications);
        m.setAllergies(allergies);
        m.setId(random.nextInt(2000));

        return Optional.ofNullable(m);
    }

    private Person createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setId(random.nextInt(2000));
        p.setFirstName(pickRandom(names) + pickRandom(names) + pickRandom(names) + pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");

        return p;
    }

    private String pickRandom(List<String> list) {
        return list.get(random.nextInt(list.size()));
    }
    
}
