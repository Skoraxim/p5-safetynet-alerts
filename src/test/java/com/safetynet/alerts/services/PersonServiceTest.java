package com.safetynet.alerts.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.annotation.Resource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import com.safetynet.alerts.model.Person;
import com.safetynet.alerts.model.CustomEndpointsObjects.ChildListWithOtherMembers;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonWithLimitedDetails;
import com.safetynet.alerts.model.CustomEndpointsObjects.PersonsDetailsByAddressForGetMappingFire;
import com.safetynet.alerts.repository.PersonsRepository;
import com.safetynet.alerts.service.PersonService;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @Mock
    PersonsRepository personsRepository;
    @InjectMocks
    @Resource
    PersonService personService;

    Random random = new Random();
    Optional<Person> p = null;
    Person person = null;





    @BeforeEach
    public void setUp() {
        p = createDummyPerson();
        person = p.get();
    }

    @Test
    public void getPersonsTest() {
        ///ARRANGE

        List<Person> persons = new ArrayList<>();
        persons.add(person);

        when(personsRepository.findAll()).thenReturn(persons);

        ///ACT///

        Person personResult = ((List<Person>) personService.getPersons()).get(0);

        ///ASSERT///

        assertEquals(true, personResult.equals(person));
    }

    @Test
    public void getPersonTest() {
        ///ARRANGE

        when(personsRepository.findById(person.getId())).thenReturn(p);

        ///ACT///

        Person resultPerson = personService.getPerson(person.getId()).get();

        ///ASSERT///

        assertEquals(true, resultPerson.equals(person));
    }

    @Test
    public void getPersonByLastNameAndFirstNameTest() {
        ///ARRANGE///

        Map<String, String> names = new HashMap<>();
        names.put("firstName", person.getFirstName());
        names.put("lastName", person.getLastName());

        when(personsRepository.findByLastNameAndFirstName(anyString(), anyString())).thenReturn(p);

        ///ACT///

        Person resultPerson = personService.getPersonByLastNameAndFirstName(names).get();

        ///ASSERT///

        assertEquals(true, resultPerson.equals(person));
    }

    @Test
    public void getPersonByLastNameAndFirstNameTest_ExceptionIfDoesNotExist() {
        ///ARRANGE///

        Map<String, String> names = new HashMap<>();
        names.put("firstName", "jmddfljefzbgfmbcvghj");
        names.put("lastName", "jhklbfoiuhjkgaesapnupuastseb");

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> personService.getPersonByLastNameAndFirstName(names));
    }

    @Test
    public void getPersonByLastNameAndFirstNameTest_ExceptionIfNotTheRightAmountOfParameters() {
        ///ARRANGE///

        Map<String, String> tooMuchNames = new HashMap<>();
        tooMuchNames.put("firstName", "jmddfljefzbgfmbcvghj");
        tooMuchNames.put("lastName", "jhklbfoiuhjkgaesapnupuastseb");
        tooMuchNames.put("favoriteFood", "Paimon");

        Map<String, String> notEnoughNames = new HashMap<>();
        notEnoughNames.put("firstName", "jmddfljefzbgfmbcvghj");

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> personService.getPersonByLastNameAndFirstName(tooMuchNames));
        assertThrows(ResponseStatusException.class, () -> personService.getPersonByLastNameAndFirstName(notEnoughNames));
    }

    @Test
    public void savePersonTest() {
        ///ARRANGE///

        when(personsRepository.save(any(Person.class))).thenReturn(person);

        ///ACT///

        Person resultPerons = personService.savePerson(person);

        ///ASSERT///

        assertEquals(true, resultPerons.equals(person));
    }

    @Test
    public void deletePersonTest() {
        ///ARRANGE///
        List<Person> persons = new ArrayList<>();
        persons.add(person);

        Map<String, String> names = new HashMap<>();
        names.put("firstName", person.getFirstName());
        names.put("lastName", person.getLastName());

        when(personsRepository.findByLastNameAndFirstName(anyString(), anyString())).thenReturn(p);

        ///ACT///

        persons.remove(person);
        personService.deletePerson(names);

        ///ASSERT///

        assertEquals(true, persons.size() == 0);
    }

    @Test
    public void deletePersonTest_ExceptionIfDoesNotExist() {
        ///ARRANGE///

        Map<String, String> names = new HashMap<>();
        names.put("firstName", "jmddfljefzbgfmbcvghj");
        names.put("lastName", "jhklbfoiuhjkgaesapnupuastseb");

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> personService.deletePerson(names));
    }

    @Test
    public void deletePersonTest_ExceptionIfNotTheRightAmountOfParameters() {
        ///ARRANGE///

        Map<String, String> tooMuchNames = new HashMap<>();
        tooMuchNames.put("firstName", "jmddfljefzbgfmbcvghj");
        tooMuchNames.put("lastName", "jhklbfoiuhjkgaesapnupuastseb");
        tooMuchNames.put("favoriteFood", "Paimon");

        Map<String, String> notEnoughNames = new HashMap<>();
        notEnoughNames.put("firstName", "jmddfljefzbgfmbcvghj");

        ///ACT & ASSERT///

        assertThrows(ResponseStatusException.class, () -> personService.deletePerson(tooMuchNames));
        assertThrows(ResponseStatusException.class, () -> personService.deletePerson(notEnoughNames));
    }

    @Test
    public void updatePersonTest() {
        ///ARRANGE///
        Optional<Person> p2 = createDummyPerson();
        Person person2 = p2.get();
        person2.setId(person.getId());

        when(personsRepository.findById(anyInt())).thenReturn(p);
        when(personsRepository.save(any(Person.class))).thenReturn(person2);

        ///ACT///

        Person resultPerson = personService.updatePerson(person.getId(), person2);

        ///ASSERT///

        assertEquals(true, resultPerson.equals(person2));
    }

    @Test
    public void setAllFieldsAsNullTest() {
        ///ACT///

        Person personResult = personService.setAllFieldsAsNull(person);

        ///ASSERTT///

        assertEquals(true, personResult.getId() == person.getId());
        assertEquals(false, personResult.equals(person));
    }

    @Test
    public void getAllMailsByCityTest() {
        ///ARRANGE///

        Optional<Person> p2 = createDummyPerson();
        Person person2 = p2.get();
        person2.setEmail("newemail@foo.oof");

        List<Person> persons = new ArrayList<>();
        persons.add(person);
        persons.add(person2);

        when(personsRepository.findByCity(anyString())).thenReturn(persons);

        ///ACT///

        List<String> emails = personService.getAllMailsByCity(person.getCity());

        ///ASSERT///

        assertEquals(true, emails.size() == 2);
        assertEquals(true, emails.contains("newemail@foo.oof"));
        assertEquals(true, emails.contains("email@liame.com"));

    }

    @Test
    public void getPersonsDetailsLimitedTest() {
        ///ARRANGE///
        Map<String, String> firstNameAndLastName = new HashMap<>();
        firstNameAndLastName.put("firstName", person.getFirstName());
        firstNameAndLastName.put("lastName", person.getLastName());

        when(personsRepository.findByLastNameAndFirstName(anyString(), anyString())).thenReturn(p);

        ///ACT///

        PersonWithLimitedDetails limitedDetailsPerson = personService.getPersonDetailsLimited(firstNameAndLastName);

        ///ASSERT///

        assertEquals(true, limitedDetailsPerson.getPerson().equals(person));
    }

    @Test
    public void getPersonsDetailsForSpecifiedAddressTest() {
        ///ARRANGE///

        Person person2 = createDummyPerson().get();
        Person person3 = createDummyPerson().get();

        List<Person> persons = new ArrayList<>();
        persons.add(person);
        persons.add(person2);
        persons.add(person3);

        when(personsRepository.findByAddress("valid address")).thenReturn(persons);
        when(personsRepository.findByAddress("random address")).thenReturn(new ArrayList<Person>());

        ///ACT///

        List<PersonsDetailsByAddressForGetMappingFire> personsDetails = personService.getPersonsDetailsForSpecifiedAddress("valid address");

        ///ASSERT///

        assertEquals(true, personsDetails.size() == 3);
        assertThrows(ResponseStatusException.class, () -> personService.getPersonsDetailsForSpecifiedAddress("random address"));
    }

    @Test
    public void getChildsAndOthersMembersByAddressTest() {
        ///ARRANGE///

        Optional<Person> p2 = createDummyPerson();
        Optional<Person> p3 = createDummyPerson();
        Person person2 = p2.get();
        Person person3 = p3.get();

        person2.setBirthDate("01/01/2020");
        person3.setAddress("3 rue des potiers");

        List<Person> addr1Persons = new ArrayList<>();
        List<Person> addr2Persons = new ArrayList<>();

        addr1Persons.add(person);
        addr1Persons.add(person2);
        addr2Persons.add(person3);

        when(personsRepository.findByAddress(person.getAddress())).thenReturn(addr1Persons);
        when(personsRepository.findByAddress(person3.getAddress())).thenReturn(addr2Persons);

        ///ACT///

        ChildListWithOtherMembers childList1 = personService.getChildsAndOthersMembersByAddress(person.getAddress());
        ChildListWithOtherMembers childList2 = personService.getChildsAndOthersMembersByAddress(person3.getAddress());

        ///ASSERT///

        assertEquals(true, childList1 != null);
        assertEquals(true, childList1.getPersons().size() == 2);
        assertEquals(true, childList2 == null);
    }





    private Optional<Person> createDummyPerson() {
        List<String> names = Arrays.asList("Antoine","Huguette", "Mélissandre", "Pierre", "Lonni", "Anne", "Renzo", "Ian", "Karina", "Fancine", "Adelaide", "Jordan", "Rudy", "Didier", "Berenice", "Thomas", "Giovanni", "Hendrick");

        Person p = new Person();

        p.setId(random.nextInt(2000));
        p.setFirstName(pickRandom(names) + pickRandom(names) + pickRandom(names));
        p.setLastName("Comas");
        p.setBirthDate("01/01/1990");
        p.setAddress("1 Rue de Paris");
        p.setCity("Paris");
        p.setZipCode("01000");
        p.setPhoneNumber("012-345-6789");
        p.setEmail("email@liame.com");

        return Optional.ofNullable(p);
    }

    private String pickRandom(List<String> list) {
        return list.get(random.nextInt(list.size()));
    }
}
