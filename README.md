# P5-SafetyNet-Alerts



## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Which version to use ?

To get a working version of the project, you can use any available one in the tags section of the branch menu (those are the releases), but we recommend the most up-to-date one.
You can also try to get a version from the develop branch, but we don't garantee you that it will work (most of the time it will not).

### Prerequisites

Here is a list of what you will need to get this project running :

- Java 1.8
- Maven 3.8.5
- MySQL 8.0.28

### Installing

Here are some guides to install Java, Maven and MySQL:

1.Install Java:

https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html

2.Install Maven:

https://maven.apache.org/install.html

3.Install MySQL:

https://dev.mysql.com/downloads/mysql/

After downloading the MySQL 8 installer and installing it, you will be asked to configure the password for the default `root` account.
This code uses the default root account to connect and the password can be set as `rootroot`. If you add another user/credentials make sure to change the same in the code base.

### Running

First of all, you will need to launch all the mysql commands in src/main/resources/Database.sql to set up the database for this project, then you can import this project in your IDE. When it's done, simply run App.java to launch the application.

### Testing

This app uses JUnit and have tests in it. To run them, open a terminal in the project directory and run `mvn test`



## Help

### Reference Documentation

For further reference, please consider the following sections :

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#web)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#data.sql.jpa-and-spring-data)

### Guides

The following guides illustrate how to use some features concretely :

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)